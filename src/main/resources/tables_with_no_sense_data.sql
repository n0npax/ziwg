-- MySQL dump 10.15  Distrib 10.0.16-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: masterOfHouse
-- ------------------------------------------------------
-- Server version	10.0.16-MariaDB-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aIs`
--

DROP TABLE IF EXISTS `aIs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aIs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aIs`
--

LOCK TABLES `aIs` WRITE;
/*!40000 ALTER TABLE `aIs` DISABLE KEYS */;
INSERT INTO `aIs` VALUES (13,'łł');
/*!40000 ALTER TABLE `aIs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buildings`
--

DROP TABLE IF EXISTS `buildings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buildings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tag` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `clat` double NOT NULL,
  `clong` double NOT NULL,
  `open_hours` varchar(50) DEFAULT NULL,
  `address` varchar(160) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `directions` varchar(120) DEFAULT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `description` varchar(600) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `www` varchar(50) DEFAULT NULL,
  `propertyLocation` int(6) NOT NULL,
  `isConfirmed` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `propertyLocation` (`propertyLocation`),
  CONSTRAINT `buildings_ibfk_1` FOREIGN KEY (`propertyLocation`) REFERENCES `propertyLocations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buildings`
--

LOCK TABLES `buildings` WRITE;
/*!40000 ALTER TABLE `buildings` DISABLE KEYS */;
INSERT INTO `buildings` VALUES (13,'złota','złota',49.872533,20.68969800000002,'złota','złota 44','biuro','nad KFC','','lala','222-333-112','','',14,1);
/*!40000 ALTER TABLE `buildings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communicationDialects`
--

DROP TABLE IF EXISTS `communicationDialects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communicationDialects` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tag` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `clat` double NOT NULL,
  `clong` double NOT NULL,
  `open_hours` varchar(50) DEFAULT NULL,
  `address` varchar(160) DEFAULT NULL,
  `floor` varchar(4) DEFAULT NULL,
  `room` varchar(6) DEFAULT NULL,
  `description` varchar(600) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `www` varchar(50) DEFAULT NULL,
  `isConfirmed` tinyint(1) DEFAULT NULL,
  `discriminator` varchar(2),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communicationDialects`
--

LOCK TABLES `communicationDialects` WRITE;
/*!40000 ALTER TABLE `communicationDialects` DISABLE KEYS */;
INSERT INTO `communicationDialects` VALUES (13,'dialect1','dialect1',51.108260661933365,17.060721343994146,'dialect1','Wittiga 4 T-16/210','','','','793600567','','',1,'C'),(1,'backdropDialect','backdropDialect',51.108260661933365,17.060721343994146,'backdropDialect','Wittiga 4 T-16/210','','','','793600567','','',1,'B');
/*!40000 ALTER TABLE `communicationDialects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tag` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `editable` tinyint(1) DEFAULT NULL,
  `description` varchar(600) DEFAULT NULL,
  `devvalues` varchar(500) DEFAULT NULL,
  `xmlDevDesc` varchar(50) DEFAULT NULL,
  `www` varchar(50) DEFAULT NULL,
  `temperatureControlling` boolean,
  `communicationDialect` int(6) NOT NULL,
  `building` int(6) NOT NULL,
  `isConfirmed` tinyint(1) DEFAULT NULL,
  `discriminator` varchar(2),
  PRIMARY KEY (`id`),
  KEY `communicationDialect` (`communicationDialect`),
  KEY `building` (`building`),
  CONSTRAINT `devices_ibfk_1` FOREIGN KEY (`communicationDialect`) REFERENCES `communicationDialects` (`id`),
  CONSTRAINT `devices_ibfk_2` FOREIGN KEY (`building`) REFERENCES `buildings` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (13,'TME','Ethernet thermometer TME',0,'lal','<thermometer><title>Ethernet thermometer TME designed by Papouch s.r.o. - www.papouch.com</title><description>TME</description><temperature>17</temperature><mintemperature>207</mintemperature><maxtemperature>260</maxtemperature></thermometer>','lala','lala',1,13,13,1,'C'),(14,'BAC','Backdrop controlled by RPi',1,'tro lolo','<backdrop><title>Backdrop controlled by RPi</title><description>Backdrop</description><position>200</position><minposition>200</minposition><maxposition>1000</maxposition></backdrop>','','',1,1,13,1,'B'),(21,'temperatureControlling1','temperatureControlling1',1,'temperatureControlling1','<device><name>temperatureControlling1</name><value>10</value></device>','temperatureControlling1','temperatureControlling1',1,13,13,1,'C'),(22,'temperatureControlling2','temperatureControlling2',1,'temperatureControlling2','<device><name>temperatureControlling2</name><value>25</value></device>','temperatureControlling2','temperatureControlling2',1,13,13,1,'C');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deviceValues`
--

DROP TABLE IF EXISTS `deviceValues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deviceValues` (
   `id` int(10) NOT NULL AUTO_INCREMENT,
   
   `date` datetime NOT NULL,
   `devValues` text NOT NULL,
   `setByUser` boolean,
   
   `device` integer(6) NOT NULL,
   
   PRIMARY KEY (`id`),
   FOREIGN KEY (device) REFERENCES devices(id)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
 
--
-- Dumping data for table `deviceValues`
--

LOCK TABLES `deviceValues` WRITE;
/*!40000 ALTER TABLE `deviceValues` DISABLE KEYS */;
INSERT INTO `deviceValues` VALUES (1,'2015-04-06 15:00:00','<device><name>abc</name><value>4</value></device>',1,13),(2,'2015-04-06 15:01:00','<device><name>abc</name><value>5</value></device>',0,13);
/*!40000 ALTER TABLE `deviceValues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `propertyLocations`
--

DROP TABLE IF EXISTS `propertyLocations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `propertyLocations` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `clat` double NOT NULL,
  `clong` double NOT NULL,
  `country` varchar(25) NOT NULL,
  `city` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `propertyLocations`
--

LOCK TABLES `propertyLocations` WRITE;
/*!40000 ALTER TABLE `propertyLocations` DISABLE KEYS */;
INSERT INTO `propertyLocations` VALUES (14,'warszawa',52.2296756,21.012228700000037,'PL','warszawa');
/*!40000 ALTER TABLE `propertyLocations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `ROLE` varchar(45) NOT NULL,
  PRIMARY KEY (`user_role_id`),
  UNIQUE KEY `uni_username_role` (`ROLE`,`username`),
  KEY `fk_username_idx` (`username`),
  CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (2,'n0npax','ROLE_ADMIN'),(3,'user','ROLE_MODERATOR'),(1,'user','ROLE_USER');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('n0npax','1a1dc91c907325c69271ddf0c944bc72',1),('user','1a1dc91c907325c69271ddf0c944bc72',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-31 18:01:51