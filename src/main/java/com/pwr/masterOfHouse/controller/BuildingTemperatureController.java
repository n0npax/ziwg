package com.pwr.masterOfHouse.controller;

import org.codehaus.jackson.JsonParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pwr.masterOfHouse.ai.TemperatureManager;
import com.pwr.masterOfHouse.repository.BuildingRepository;

@Controller
public class BuildingTemperatureController {

	@Autowired
	private TemperatureManager manager;
	@Autowired
	private BuildingRepository buildingRepo;

	@RequestMapping(value = "/buildingTemperature", method = RequestMethod.POST, produces = MediaType.ALL_VALUE, consumes = MediaType.ALL_VALUE)
	@ResponseBody
	public String setBuildingTemperature(@RequestBody String temperature) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject obj = (JSONObject) parser.parse(temperature);
		String buildingTemperature = (String) obj.get("buildingTemperature");
		String building = (String) obj.get("building");
		manager.setTemperatureForBuilding(
				buildingRepo.findOne(Integer.parseInt(building)),
				Integer.parseInt(buildingTemperature));
		return "OK";
	}

}
