package com.pwr.masterOfHouse.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.castor.CastorMarshaller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.pwr.masterOfHouse.model.PropertyLocation;

import com.pwr.masterOfHouse.service.PropertyLocationService;

@Controller
@RequestMapping(value = "/propertyLocations")
@PropertySource("classpath:application.properties")

public class PropertyLocationController {


    @Autowired
    private PropertyLocationService propertyLocationsService;



    @Autowired
    private CastorMarshaller marschaller;

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView createPropertyLocationPage() {
        ModelAndView mav = new ModelAndView("propertyLocations/new-propertyLocation");
        mav.addObject("propertyLocation", new PropertyLocation());
        return mav;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PropertyLocation createPropertyLocation(@RequestBody PropertyLocation propertyLocation) {
        return propertyLocationsService.create(propertyLocation);
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PropertyLocation editBuilding(@RequestBody PropertyLocation building,
                                         @PathVariable int id) {
        building.setId(id);
        return propertyLocationsService.update(building);
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPropertyLocationPage(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("propertyLocations/edit-propertyLocation");
        PropertyLocation b = propertyLocationsService.get(id);
        mav.addObject("propertyLocation", b);
        return mav;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PropertyLocation deletePropertyLocation(@PathVariable int id) {
        return propertyLocationsService.delete(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView allPropertyLocationsPage() {
        ModelAndView mav = new ModelAndView("propertyLocations/all-propertyLocation");
        List<PropertyLocation> propertyLocations = new ArrayList<PropertyLocation>();
        propertyLocations.addAll(propertyLocationsService.getAll());
        Collections.sort(propertyLocations);
        mav.addObject("propertyLocations", propertyLocations);
        return mav;
    }

    @RequestMapping(value = "/xml/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public PropertyLocation showBuildingXml(@PathVariable int id) {
        return propertyLocationsService.get(id);
    }







}
