package com.pwr.masterOfHouse.controller;

/**
 * Created by n0npax on 3/14/15.
 */
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class LoginController {
    @RequestMapping(value = {"/login.do","/"}, method = RequestMethod.GET)
    public String showLogin() {
        return "loginPage";
    }

}
