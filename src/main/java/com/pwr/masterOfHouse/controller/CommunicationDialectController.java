package com.pwr.masterOfHouse.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.pwr.masterOfHouse.model.BasicCommunicationDialect;
import com.pwr.masterOfHouse.model.CommunicationDialect;
import com.pwr.masterOfHouse.service.CommunicationDialectService;
import com.pwr.masterOfHouse.service.PropertyLocationService;

@Controller
@RequestMapping(value = "/communicationDialects")
public class CommunicationDialectController {

    @Autowired
    private CommunicationDialectService communicationDialectService;
    @Autowired
    private PropertyLocationService propertyLocationService;

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public ModelAndView allPropertyLocationsPage() {
        ModelAndView mav = new ModelAndView("communicationDialects/create");
        mav.addObject("communicationDialect", new BasicCommunicationDialect());
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CommunicationDialect createCommunicationDialect(@RequestBody CommunicationDialect communicationDialect) {
        return communicationDialectService.create(communicationDialect);
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CommunicationDialect editCommunicationDialect(@PathVariable int id, @RequestBody CommunicationDialect communicationDialect) {
        communicationDialect.setId(id);
        return communicationDialectService.update(communicationDialect);
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editCommunicationDialectPage(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("communicationDialects/edit-communicationDialect");
        CommunicationDialect communicationDialect = communicationDialectService.get(id);
        mav.addObject("communicationDialect", communicationDialect);
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CommunicationDialect deleteCommunicationDialect(@PathVariable int id) {
        return communicationDialectService.delete(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView allCommunicationDialectsPage() {
        ModelAndView mav = new ModelAndView("communicationDialects/all-communicationDialects");
        List<CommunicationDialect> communicationDialects = new ArrayList<CommunicationDialect>();
        communicationDialects.addAll(communicationDialectService.getAll());
        Collections.sort(communicationDialects);
        mav.addObject("communicationDialects", communicationDialects);
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;
    }

    @RequestMapping(value = "/xml/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public CommunicationDialect showBuildingXml(@PathVariable int id) {
        return communicationDialectService.get(id);
    }

    @RequestMapping(value = "/allCommunicationDialectsFromPropertyLocation/{id}", method = RequestMethod.GET)
    public ModelAndView communicationDialectsFromPropertyLocation(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("communicationDialects/all-communicationDialects");
        List<CommunicationDialect> communicationDialects = new ArrayList<CommunicationDialect>();
        communicationDialects.addAll(communicationDialectService.getCommunicationDialectsFromPropertyLocation(id));
        Collections.sort(communicationDialects);
        mav.addObject("communicationDialects", communicationDialects);
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;
    }

    @RequestMapping(value = "/toConfirm/{id}", method = RequestMethod.GET)
    public ModelAndView communicationDialectsFromPropertyLocationToConfirm(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("communicationDialects/communicationDialects_to_confirm");
        List<CommunicationDialect> communicationDialects = new ArrayList<CommunicationDialect>();
        communicationDialects.addAll(communicationDialectService.getCommunicationDialectsFromPropertyLocation(id, false));
        mav.addObject("communicationDialects", communicationDialects);
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;
    }

    @RequestMapping(value = "/confirm/{id}", method = RequestMethod.GET)
    public ModelAndView confirmCommunicationDialect(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("communicationDialects/communicationDialects_to_confirm");
        CommunicationDialect communicationDialect = communicationDialectService.get(id);
        communicationDialectService.update(communicationDialect);
        List<CommunicationDialect> communicationDialects = new ArrayList<CommunicationDialect>();
        communicationDialects.addAll(communicationDialectService.getCommunicationDialectsFromPropertyLocation(id, false));

        mav.addObject("communicationDialects", communicationDialects);
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;



    }

}
