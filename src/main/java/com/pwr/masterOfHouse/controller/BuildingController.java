package com.pwr.masterOfHouse.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.pwr.masterOfHouse.model.Building;
import com.pwr.masterOfHouse.service.BuildingService;
import com.pwr.masterOfHouse.service.PropertyLocationService;

@Controller
@RequestMapping(value = "/buildings")
public class BuildingController {

    @Autowired
    private BuildingService buildingService;
    @Autowired
    private PropertyLocationService propertyLocationService;

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView createBuildingPage() {
        ModelAndView mav = new ModelAndView("buildings/new-building");
        mav.addObject("building", new Building());
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;

    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Building createBuilding(@RequestBody Building building) {
        return buildingService.create(building);
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Building editBuilding(@RequestBody Building building,
                                 @PathVariable int id) {
        building.setId(id);
        return buildingService.update(building);
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editBuildingPage(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("buildings/edit-building");
        Building b = buildingService.get(id);
        mav.addObject("building", b);
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Building deleteBuilding(@PathVariable int id) {
        return buildingService.delete(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView allBuildingsPage() {
        ModelAndView mav = new ModelAndView("buildings/all-buildings");
        List<Building> buildings = new ArrayList<Building>();
        buildings.addAll(buildingService.getAll());
        Collections.sort(buildings);
        mav.addObject("buildings", buildings);
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;
    }

    @RequestMapping(value = "/xml/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public Building showBuildingXml(@PathVariable int id) {
        return buildingService.get(id);
    }

    @RequestMapping(value = "/buildingsFromPropertyLocation/{id}", method = RequestMethod.GET)
    public ModelAndView buildingsFromPropertyLocation(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("buildings/all-buildings");
        List<Building> buildings = new ArrayList<Building>();
        buildings.addAll(buildingService.getBuildingsFromPropertyLocation(id));
        Collections.sort(buildings);
        mav.addObject("buildings", buildings);
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;
    }

    @RequestMapping(value = "/toConfirm/{id}", method = RequestMethod.GET)
    public ModelAndView buildingsFromPropertyLocationToConfirm(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("buildings/buildings_to_confirm");
        List<Building> buildings = new ArrayList<Building>();
        buildings.addAll(buildingService.getBuildingsFromPropertyLocation(id, false));
        mav.addObject("buildings", buildings);
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;
    }

    @RequestMapping(value = "/confirm/{id}", method = RequestMethod.GET)
    public ModelAndView confirmBuilding(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("buildings/buildings_to_confirm");
        Building building = buildingService.get(id);
        buildingService.update(building);
        List<Building> buildings = new ArrayList<Building>();
        buildings.addAll(buildingService.getBuildingsFromPropertyLocation(
                building.getPropertyLocation(), false));
        mav.addObject("buildings", buildings);
        mav.addObject("univ", propertyLocationService.getAll());
        return mav;
    }
}
