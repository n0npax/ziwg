package com.pwr.masterOfHouse.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.pwr.masterOfHouse.model.Device;
import com.pwr.masterOfHouse.model.DeviceMapper;
import com.pwr.masterOfHouse.repository.DeviceValuesRepository;
import com.pwr.masterOfHouse.service.BuildingService;
import com.pwr.masterOfHouse.service.CommunicationDialectService;
import com.pwr.masterOfHouse.service.DeviceService;

@Controller
@RequestMapping(value = "/devices")
public class DeviceController {

	@Autowired
	private DeviceService deviceService;
	@Autowired
	private BuildingService buildingService;
	@Autowired
	private CommunicationDialectService communicationDialectService;
	@Autowired
	private DeviceValuesRepository deviceValuesRepository;

	@RequestMapping(value = "create", method = RequestMethod.GET)
	public ModelAndView allBuildingsPage() {
		ModelAndView mav = new ModelAndView("devices/create");
		mav.addObject("device", new Device());
		mav.addObject("univ", buildingService.getAll());
		mav.addObject("comd", communicationDialectService.getAll());
		return mav;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Device createDevice(@RequestBody DeviceMapper dev) {
		Device device = new Device(dev);
		device.setBuilding(buildingService.get(dev.getBuilding()));
		device.setCommunicationDialect(communicationDialectService.get(dev
				.getCommunicationDialect()));
		return deviceService.create(device);
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Device editDevice(@PathVariable int id, @RequestBody DeviceMapper dev) {
		Device device = new Device(dev);
		device.setBuilding(buildingService.get(dev.getBuilding()));
		device.setCommunicationDialect(communicationDialectService.get(dev
				.getCommunicationDialect()));
		device.setId(id);
		return deviceService.update(device);
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editDevicePage(@PathVariable int id) {
		ModelAndView mav = new ModelAndView("devices/edit-device");
		Device device = deviceService.get(id);
		mav.addObject("device", device);
		mav.addObject("univ", buildingService.getAll());
		mav.addObject("comd", communicationDialectService.getAll());
		return mav;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Device deleteDevice(@PathVariable int id) {
		deviceValuesRepository.deleteByDevice(deviceService.get(id));
		return deviceService.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView allDevicesPage() {
		ModelAndView mav = new ModelAndView("devices/all-devices");
		List<Device> devices = new ArrayList<Device>();
		devices.addAll(deviceService.getAll());
		Collections.sort(devices);
		mav.addObject("devices", devices);
		mav.addObject("univ", buildingService.getAll());
		mav.addObject("comd", communicationDialectService.getAll());
		return mav;
	}

	@RequestMapping(value = "/xml/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	@ResponseBody
	public Device showBuildingXml(@PathVariable int id) {
		return deviceService.get(id);
	}

	@RequestMapping(value = "/allDevicesFromBuilding/{id}", method = RequestMethod.GET)
	public ModelAndView devicesFromBuilding(@PathVariable int id) {
		ModelAndView mav = new ModelAndView("devices/all-devices");
		List<Device> devices = new ArrayList<Device>();
		devices.addAll(deviceService.getDevicesFromBuilding(buildingService
				.get(id)));
		Collections.sort(devices);
		mav.addObject("devices", devices);
		mav.addObject("univ", buildingService.getAll());
		mav.addObject("comd", communicationDialectService.getAll());
		mav.addObject("activeBuilding", id);
		return mav;
	}

	@RequestMapping(value = "/toConfirm/{id}", method = RequestMethod.GET)
	public ModelAndView devicesFromBuildingToConfirm(@PathVariable int id) {
		ModelAndView mav = new ModelAndView("devices/devices_to_confirm");
		List<Device> devices = new ArrayList<Device>();
		devices.addAll(deviceService.getDevicesFromBuilding(
				buildingService.get(id), false));
		mav.addObject("devices", devices);
		mav.addObject("univ", buildingService.getAll());
		mav.addObject("comd", communicationDialectService.getAll());
		return mav;
	}

	@RequestMapping(value = "/confirm/{id}", method = RequestMethod.GET)
	public ModelAndView confirmDevice(@PathVariable int id) {
		ModelAndView mav = new ModelAndView("devices/devices_to_confirm");
		Device device = deviceService.get(id);
		deviceService.update(device);
		List<Device> devices = new ArrayList<Device>();
		devices.addAll(deviceService.getDevicesFromBuilding(
				device.getBuilding(), false));
		mav.addObject("devices", devices);
		mav.addObject("univ", buildingService.getAll());
		mav.addObject("comd", communicationDialectService.getAll());
		return mav;
	}

}
