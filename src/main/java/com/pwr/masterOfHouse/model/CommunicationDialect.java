package com.pwr.masterOfHouse.model;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "communicationDialects")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "discriminator", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("P")
@XmlRootElement(name = "communicationDialect")
@XmlAccessorType(XmlAccessType.NONE)
public abstract class CommunicationDialect implements Comparable<CommunicationDialect> {

    @Id
    @GeneratedValue
    private Integer id;
    @XmlElement
    private String tag;
    @XmlElement
    private String name;
    @XmlElement(name = "lat")
    private Double clat;
    @XmlElement(name = "long")
    private Double clong;
    @XmlElement
    private String open_hours;
    @XmlElement
    private String address;
    @XmlElement
    private String floor;
    @XmlElement
    private String room;
    @XmlElement
    private String description;
    @XmlElement
    private String phone;
    @XmlElement
    private String mail;
    @XmlElement
    private String www;
    private Boolean isConfirmed;

    public CommunicationDialect update(CommunicationDialect communicationDialect) {
        this.tag = communicationDialect.tag;
        this.name = communicationDialect.name;
        this.clat = communicationDialect.clat;
        this.clong = communicationDialect.clong;
        this.open_hours = communicationDialect.open_hours;
        this.address = communicationDialect.address;
        this.floor = communicationDialect.floor;
        this.room = communicationDialect.room;
        this.description = communicationDialect.description;
        this.phone = communicationDialect.phone;
        this.mail = communicationDialect.mail;
        this.www = communicationDialect.www;
        this.isConfirmed = communicationDialect.isConfirmed;
        return this;
    }

    @Override
    public String toString() {
        String str = "";
        str += this.tag + " ";
        str += this.name + " ";
        str += this.clat.toString() + " ";
        str += this.clong.toString() + " ";
        str += this.open_hours + " ";
        str += this.address + " ";
        str += this.floor + " ";
        str += this.room + " ";
        str += this.description + " ";
        str += this.phone + " ";
        str += this.mail + " ";
        str += this.www + " ";
        str += this.isConfirmed;
        return str;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getClat() {
        return clat;
    }

    public void setClat(Double clat) {
        this.clat = clat;
    }

    public Double getClong() {
        return clong;
    }

    public void setClong(Double clong) {
        this.clong = clong;
    }

    public String getOpen_hours() {
        return open_hours;
    }

    public void setOpen_hours(String open_hours) {
        this.open_hours = open_hours;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public Boolean getIsConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(Boolean isConfirmed) {
        this.isConfirmed = isConfirmed;
    }

    @Override
    public int compareTo(CommunicationDialect o) {
        return tag.compareToIgnoreCase(o.tag);
    }

	public abstract void nonGenericSet(Device device, int value, int currentTemperature);

	public abstract double getTemperature(Device device);

}