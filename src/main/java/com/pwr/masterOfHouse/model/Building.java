package com.pwr.masterOfHouse.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "buildings")
@XmlRootElement(name = "building")
@XmlAccessorType(XmlAccessType.NONE)
public class Building implements Comparable<Building> {

    @Id
    @GeneratedValue
    private Integer id;
    @XmlElement
    private String tag;
    @XmlElement
    private String name;
    @XmlElement(name = "lat")
    private Double clat;
    @XmlElement(name = "long")
    private Double clong;
    @XmlElement
    private String open_hours;
    @XmlElement
    private String address;
    @XmlElement
    private String department;
    @XmlElement
    private String directions;
    @XmlElement
    private String photo;
    @XmlElement
    private String description;
    @XmlElement
    private String phone;
    @XmlElement
    private String mail;
    @XmlElement
    private String www;
    private Integer propertyLocation;
    private Boolean isConfirmed;

    public Building update(Building building) {
        this.tag = building.tag;
        this.name = building.name;
        this.clat = building.clat;
        this.clong = building.clong;
        this.open_hours = building.open_hours;
        this.address = building.address;
        this.department = building.department;
        this.directions = building.directions;
        this.photo = building.photo;
        this.description = building.description;
        this.phone = building.phone;
        this.mail = building.mail;
        this.www = building.www;
        this.propertyLocation = building.propertyLocation;
        this.isConfirmed = building.isConfirmed;
        return this;
    }

    @Override
    public String toString() {
        String str = "";
        str += this.tag + " ";
        str += this.name + " ";
        str += this.clat.toString() + " ";
        str += this.clong.toString() + " ";
        str += this.open_hours + " ";
        str += this.address + " ";
        str += this.department + " ";
        str += this.directions + " ";
        str += this.photo + " ";
        str += this.description + " ";
        str += this.phone + " ";
        str += this.mail + " ";
        str += this.www + " ";
        str += this.propertyLocation;
        str += this.isConfirmed;
        return str;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getClat() {
        return clat;
    }

    public void setClat(Double clat) {
        this.clat = clat;
    }

    public Double getClong() {
        return clong;
    }

    public void setClong(Double clong) {
        this.clong = clong;
    }

    public String getOpen_hours() {
        return open_hours;
    }

    public void setOpen_hours(String open_hours) {
        this.open_hours = open_hours;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public Integer getPropertyLocation() {
        return propertyLocation;
    }

    public void setPropertyLocation(Integer propertyLocation) {
        this.propertyLocation = propertyLocation;
    }

    public Boolean getIsConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(Boolean isConfirmed) {
        this.isConfirmed = isConfirmed;
    }

    @Override
    public int compareTo(Building o) {
        return tag.compareToIgnoreCase(o.tag);
    }

}