package com.pwr.masterOfHouse.model;

/**
 * Created by Piotr on 2015-06-10.
 */

public class StringContainer
{
    private String node;
    private String elements;

    public void setNode (String node)
    {
        this.node = node;
    }

    public void setElements (String elements)
    {
        this.elements = elements;
    }

    public String getNode ()
    {
        return node;
    }

    public String getElements ()
    {
        return elements;
    }

    public StringContainer (String node, String elements)
    {
        this.node = node;
        this.elements = elements;
    }
}
