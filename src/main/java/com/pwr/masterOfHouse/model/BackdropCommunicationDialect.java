package com.pwr.masterOfHouse.model;

import java.io.StringReader;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

@Entity(name = "BackdropCommunicationDialect")
@DiscriminatorValue("B")
public class BackdropCommunicationDialect extends CommunicationDialect {

	/**
	 * In a temperature communication dialect - a temperature can be set, so we
	 * assume that the argument is a tempearture to set
	 */
	@Override
	public void nonGenericSet(Device device, int value, int currentTemperature) {
		int minPosition = 0;
		int maxPosition = 0;
		int position = 0;
		try {
			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();
			String devValues = device.getDevValues();
			InputSource source = new InputSource(new StringReader(devValues));
			String minPositionText = xpath.evaluate("/backdrop/minposition", source);
			source = new InputSource(new StringReader(devValues));
			String maxPositionText = xpath.evaluate("/backdrop/maxposition", source);
			minPosition = Integer.parseInt(minPositionText);
			maxPosition = Integer.parseInt(maxPositionText);
		} catch (Exception e) {
			return;
		}
		if (currentTemperature < value) {
			position = maxPosition;
		} else {
			position = minPosition;
		}
		String oldValues = device.getDevValues();
		// regex to find <position>number</position> - the replaceAll method
		// changes
		// this to new temperature value
		String newValues = oldValues.replaceAll(
				"<value>[-+]?([0-9]*\\.[0-9]+|[0-9]+)<\\/value>",
				"<value>" + Integer.toString(position) + "</value>");
		device.setDevValues(newValues);
	}

	@Override
	public double getTemperature(Device device) {
		return -10000;
	}
}
