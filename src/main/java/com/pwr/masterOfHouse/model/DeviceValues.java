package com.pwr.masterOfHouse.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "deviceValues")
@XmlRootElement(name = "deviceValue")
@XmlAccessorType(XmlAccessType.NONE)
public class DeviceValues {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;
	@XmlElement
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date")
	private Date date;
	@XmlElement
	@Column(name = "devValues")
	private String devvalues;
	@XmlElement
	@Column(name = "setByUser")
	private boolean setByUser;
	@ManyToOne
	@JoinColumn(name = "device")
	private Device device;
	
	public DeviceValues update(DeviceValues deviceValues) {
		this.id = deviceValues.id;
		this.date = deviceValues.date;
		this.devvalues = deviceValues.devvalues;
		this.setByUser = deviceValues.setByUser;
		this.device = deviceValues.device;
		return this;
	}
	
	@Override
	public String toString() {
        String str = "";
        str += this.date + " ";
        str += this.devvalues + " ";
        str += this.setByUser + " ";
        str += this.device + " ";
        return str;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getValues() {
		return devvalues;
	}

	public void setValues(String values) {
		this.devvalues = values;
	}

	public boolean isSetByUser() {
		return setByUser;
	}

	public void setSetByUser(boolean setByUser) {
		this.setByUser = setByUser;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

}
