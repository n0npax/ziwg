package com.pwr.masterOfHouse.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "propertyLocations")
@XmlRootElement(name = "propertyLocation")
@XmlAccessorType(XmlAccessType.NONE)
public class PropertyLocation implements Comparable<PropertyLocation> {

    @Id
    @GeneratedValue
    private Integer id;

    @XmlElement
    private String name;
    @XmlElement
    private String country;
    @XmlElement
    private String city;
    @XmlElement(name = "lat")
    private Double clat;
    @XmlElement(name = "long")
    private Double clong;

    public PropertyLocation update(PropertyLocation propertyLocation) {
        this.name = propertyLocation.name;
        this.country = propertyLocation.country;
        this.city = propertyLocation.city;
        this.clat = propertyLocation.clat;
        this.clong = propertyLocation.clong;
        return this;
    }

    @Override
    public String toString() {
        return name + ": " + country + " " + city;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getClat() {
        return clat;
    }

    public void setClat(Double clat) {
        this.clat = clat;
    }

    public Double getClong() {
        return clong;
    }

    public void setClong(Double clong) {
        this.clong = clong;
    }

    @Override
    public int compareTo(PropertyLocation o) {
        return name.compareToIgnoreCase(o.name);
    }

}
