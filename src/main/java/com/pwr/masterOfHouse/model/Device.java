package com.pwr.masterOfHouse.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.pwr.masterOfHouse.ai.HistoryComplementer;
import com.pwr.masterOfHouse.ctxProvider.ApplicationContextProvider;


import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "devices")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "discriminator", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("P")
@XmlRootElement(name = "device")
@XmlAccessorType(XmlAccessType.NONE)
public class Device implements Comparable<Device> {

	transient HistoryComplementer history = null;

	@Id
	@GeneratedValue
	private Integer id;
	@XmlElement
	@Column(name = "tag")
	private String tag;
	@XmlElement
	@Column(name = "name")
	private String name;
	@XmlElement
	@Column(name = "editable")
	private Boolean editable;
	@XmlElement
	@Column(name = "description")
	private String description;
	@XmlElement
	@Column(name = "devvalues")
	private String devValues;
	@XmlElement
	@Column(name = "xmlDevDesc")
	private String xmlDevDesc;
	@XmlElement
	@Column(name = "www")
	private String www;
	@XmlElement
	@Column(name = "temperatureControlling")
	private boolean temperatureControlling;
	@ManyToOne
	@JoinColumn(name = "communicationDialect")
	private CommunicationDialect communicationDialect;
	@ManyToOne
	@JoinColumn(name = "building")
	private Building building;
	@Column(name = "isConfirmed")
	private Boolean isConfirmed;

	public Device(DeviceMapper device) {
		this.tag = device.getTag();
		this.name = device.getName();
		this.editable = device.getEditable();
		this.description = device.getDescription();
		this.devValues = device.getDevValues();
		this.xmlDevDesc = device.getXmlDevDesc();
		this.www = device.getWww();
		this.isConfirmed = device.getIsConfirmed();
	}

	public String getDevValues2()throws Exception
	{
		Map <Integer, StringContainer> map = new HashMap <Integer, StringContainer>();

		map.put(14, new StringContainer("backdrop", "value"));
		map.put(21, new StringContainer("device", "value"));
		map.put(22, new StringContainer("device", "value"));
		map.put(13, new StringContainer("thermometer", "value"));

		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(this.devValues));

		Document doc = db.parse(is);
		NodeList nodes = doc.getElementsByTagName(map.get(this.id).getNode());

		String result = "";

		for (int i = 0; i < nodes.getLength(); i++) {
			Element element = (Element) nodes.item(i);

			NodeList name = element.getElementsByTagName(map.get(this.id).getElements());
			Element line = (Element) name.item(0);
			result = getCharacterDataFromElement(line);
		}
		return result;
	}

	public String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	public Device() {
	}

	public Device update(Device device) {
		this.tag = device.tag;
		this.name = device.name;
		this.editable = device.editable;
		this.description = device.description;
		this.devValues = device.devValues;
		this.xmlDevDesc = device.xmlDevDesc;
		this.www = device.www;
		this.building = device.building;
		this.isConfirmed = device.isConfirmed;
		return this;
	}

	@Override
	public String toString() {
		String str = "";
		str += this.tag + " ";
		str += this.name + " ";
		str += this.editable + " ";
		str += this.description + " ";
		str += this.devValues + " ";
		str += this.xmlDevDesc + " ";
		str += this.www + " ";
		str += this.building;
		str += this.isConfirmed;
		return str;
	}

	public CommunicationDialect getCommunicationDialect() {
		return communicationDialect;
	}

	public void setCommunicationDialect(
			CommunicationDialect communicationDialect) {
		this.communicationDialect = communicationDialect;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDevValues() {
		if (devValues == null)
			return null;
		if (history == null)
			history = (HistoryComplementer) ApplicationContextProvider
					.getApplicationContext().getBean("historyComplementer",
							HistoryComplementer.class);
		history.persistReadFromDevice(devValues, this);
		return devValues;
	}

	public void setDevValues(String devValues) {
		if (devValues != null && this.id != null) {
			if (history == null)
				history = (HistoryComplementer) ApplicationContextProvider
						.getApplicationContext().getBean("historyComplementer",
								HistoryComplementer.class);
			history.persistSetByUser(devValues, this);
		}
		this.devValues = devValues;
	}

	public String getXmlDevDesc() {
		return xmlDevDesc;
	}

	public void setXmlDevDesc(String xmlDevDesc) {
		this.xmlDevDesc = xmlDevDesc;
	}

	public String getWww() {
		return www;
	}

	public void setWww(String www) {
		this.www = www;
	}

	public Building getBuilding() {
		return building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}

	public Boolean getIsConfirmed() {
		return isConfirmed;
	}

	public void setIsConfirmed(Boolean isConfirmed) {
		this.isConfirmed = isConfirmed;
	}

	@Override
	public int compareTo(Device o) {
		return tag.compareToIgnoreCase(o.tag);
	}

	public boolean isTemperatureControlling() {
		return temperatureControlling;
	}

	public void setTemperatureControlling(boolean temperatureControlling) {
		this.temperatureControlling = temperatureControlling;
	}

}
