package com.pwr.masterOfHouse.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.pwr.masterOfHouse.ai.HistoryComplementer;

@XmlRootElement(name = "device")
@XmlAccessorType(XmlAccessType.NONE)
public class DeviceMapper implements Comparable<DeviceMapper> {

	transient HistoryComplementer history = null;

	private Integer id;
	@XmlElement
	private String tag;
	@XmlElement
	private String name;
	@XmlElement
	private Boolean editable;
	@XmlElement
	private String description;
	@XmlElement
	private String devValues;
	@XmlElement
	private String xmlDevDesc;
	@XmlElement
	private String www;
	@XmlElement
	private boolean temperatureControlling;
	private Integer communicationDialect;
	private Integer building;
	private Boolean isConfirmed;

	@Override
	public String toString() {
		String str = "";
		str += this.tag + " ";
		str += this.name + " ";
		str += this.editable + " ";
		str += this.description + " ";
		str += this.devValues + " ";
		str += this.xmlDevDesc + " ";
		str += this.www + " ";
		str += this.building;
		str += this.isConfirmed;
		return str;
	}

	public Integer getCommunicationDialect() {
		return communicationDialect;
	}

	public void setCommunicationDialect(Integer communicationDialect) {
		this.communicationDialect = communicationDialect;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDevValues() {
		return devValues;
	}

	public void setDevValues(String devValues) {
		this.devValues = devValues;
	}

	public String getXmlDevDesc() {
		return xmlDevDesc;
	}

	public void setXmlDevDesc(String xmlDevDesc) {
		this.xmlDevDesc = xmlDevDesc;
	}

	public String getWww() {
		return www;
	}

	public void setWww(String www) {
		this.www = www;
	}

	public Integer getBuilding() {
		return building;
	}

	public void setBuilding(Integer building) {
		this.building = building;
	}

	public Boolean getIsConfirmed() {
		return isConfirmed;
	}

	public void setIsConfirmed(Boolean isConfirmed) {
		this.isConfirmed = isConfirmed;
	}

	@Override
	public int compareTo(DeviceMapper o) {
		return tag.compareToIgnoreCase(o.tag);
	}

	public boolean isTemperatureControlling() {
		return temperatureControlling;
	}

	public void setTemperatureControlling(boolean temperatureControlling) {
		this.temperatureControlling = temperatureControlling;
	}

}
