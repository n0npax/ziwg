package com.pwr.masterOfHouse.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "BackdropControllingDevice")
@DiscriminatorValue("B")
public class Backdrop extends TemperatureControllingDevice {

	public void setTemperature(int temperature, int currentTemperature) {
		this.getCommunicationDialect().nonGenericSet(this, temperature,
				currentTemperature);
	}

	public double getTemperature() {
		return -10000;
	}

}
