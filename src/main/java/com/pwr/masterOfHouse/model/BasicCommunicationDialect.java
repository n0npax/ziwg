package com.pwr.masterOfHouse.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "BasicCommunicationDialect")
@DiscriminatorValue("C")
public class BasicCommunicationDialect extends CommunicationDialect {

	@Override
	public void nonGenericSet(Device device, int value, int currentTemperature) {
		// nothing to do in a basic communication dialect
	}

	@Override
	public double getTemperature(Device device) {
		// nothing to do in a basic communication dialect
		return 0;
	}

}
