package com.pwr.masterOfHouse.model;

import java.io.StringReader;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.xml.sax.InputSource;

@Entity(name = "TemperatureCommunicationDialect")
@DiscriminatorValue("C")
public class TemperatureCommunicationDialect extends CommunicationDialect {

	/**
	 * In a temperature communication dialect - a temperature can be set, so we
	 * assume that the argument is a tempearture to set
	 */
	@Override
	public void nonGenericSet(Device device, int value, int currentTemperature) {
		TemperatureControllingDevice dev = (TemperatureControllingDevice) device;
		String oldValues = dev.getDevValues();
		// regex to find <value>number</value> - the replaceAll method changes
		// this to new temperature value
		String newValues = oldValues.replaceAll(
				"<value>[-+]?([0-9]*\\.[0-9]+|[0-9]+)<\\/value>", "<value>"
						+ Double.toString(value) + "</value>");
		dev.setDevValues(newValues);
	}

	@Override
	public double getTemperature(Device device) {
		String temperature = "-10000";
		try {
			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();
			String devValues = device.getDevValues();
			InputSource source = new InputSource(new StringReader(devValues));
			temperature = xpath.evaluate("/thermometer/value", source);
			return Double.parseDouble(temperature);
		} catch (Exception e) {
			return -10000;
		}
	}
}
