package com.pwr.masterOfHouse.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "TemperatureControllingDevice")
@DiscriminatorValue("C")
public class TemperatureControllingDevice extends Device {

	public void setTemperature(int temperature, int currentTemperature) {
		this.getCommunicationDialect().nonGenericSet(this, temperature,
				currentTemperature);
	}

	public double getTemperature() {
		return this.getCommunicationDialect().getTemperature(this);
	}

}
