package com.pwr.masterOfHouse.repository;

import java.util.List;

import com.pwr.masterOfHouse.model.Building;
import com.pwr.masterOfHouse.model.TemperatureControllingDevice;

public interface AdvancedDeviceRepository {

	public List<TemperatureControllingDevice> getDevicesForTemperatureControlling(
			Building building);

}
