package com.pwr.masterOfHouse.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pwr.masterOfHouse.model.Device;
import com.pwr.masterOfHouse.model.DeviceValues;

public interface DeviceValuesRepository extends
		JpaRepository<DeviceValues, Integer>, AdvancedDeviceValuesRepository {

	public List<DeviceValues> findByDeviceOrderByDateDesc(Device device);

	public List<DeviceValues> findByDeviceAndSetByUserTrueOrderByDateDesc(
			Device device);

	public List<DeviceValues> findByDeviceAndSetByUserFalseOrderByDateDesc(
			Device device);

}
