package com.pwr.masterOfHouse.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pwr.masterOfHouse.model.Building;

public interface BuildingRepository extends JpaRepository<Building, Integer> {

    @Query("select b from Building b where propertyLocation=?1")
    public List<Building> getBuildingsFromPropertyLocation(Integer propertyLocationId);

    @Query("select b from Building b where propertyLocation=?1 and isConfirmed=?2")
    public List<Building> getBuildingsFromPropertyLocation(Integer propertyLocationId,
                                                           Boolean isConfirmed);

}
