package com.pwr.masterOfHouse.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.pwr.masterOfHouse.model.Building;
import com.pwr.masterOfHouse.model.Device;
import com.pwr.masterOfHouse.model.TemperatureControllingDevice;

public class DeviceRepositoryImpl implements AdvancedDeviceRepository {

	@Autowired
	DeviceRepository repo;

	@Override
	public List<TemperatureControllingDevice> getDevicesForTemperatureControlling(
			Building building) {
		List<TemperatureControllingDevice> devices = new ArrayList<TemperatureControllingDevice>();
		List<Device> allDevices = repo.getDevicesFromBuilding(building);
		for (Device device : allDevices) {
			if (device.isTemperatureControlling()) {
				devices.add((TemperatureControllingDevice) device);
			}
		}
		return devices;
	}
}
