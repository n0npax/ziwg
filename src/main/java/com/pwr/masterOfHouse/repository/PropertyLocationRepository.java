package com.pwr.masterOfHouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwr.masterOfHouse.model.PropertyLocation;

public interface PropertyLocationRepository extends JpaRepository<PropertyLocation, Integer> {

}
