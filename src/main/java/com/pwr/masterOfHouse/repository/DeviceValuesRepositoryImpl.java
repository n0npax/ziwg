package com.pwr.masterOfHouse.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.pwr.masterOfHouse.model.Device;
import com.pwr.masterOfHouse.model.DeviceValues;

public class DeviceValuesRepositoryImpl implements
		AdvancedDeviceValuesRepository {

	@Autowired
	DeviceValuesRepository repo;

	@Override
	public DeviceValues currentValueForDevice(Device device) {
		return repo.findByDeviceAndSetByUserFalseOrderByDateDesc(device).get(0);
	}

	@Override
	public DeviceValues userSettingForDevice(Device device) {
		return repo.findByDeviceAndSetByUserTrueOrderByDateDesc(device).get(0);
	}

	@Override
	public void deleteByDevice(Device device) {
		List<DeviceValues> devices = repo.findByDeviceOrderByDateDesc(device);
		for (DeviceValues item : devices) {
			repo.delete(item);
		}
	}

}
