package com.pwr.masterOfHouse.repository;

import java.util.List;

import com.pwr.masterOfHouse.model.Device;
import com.pwr.masterOfHouse.model.DeviceValues;

public interface AdvancedDeviceValuesRepository {

	DeviceValues currentValueForDevice(Device device);

	DeviceValues userSettingForDevice(Device device);
	
	public void deleteByDevice(Device device);

}
