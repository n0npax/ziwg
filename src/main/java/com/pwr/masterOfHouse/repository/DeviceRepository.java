package com.pwr.masterOfHouse.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pwr.masterOfHouse.model.Building;
import com.pwr.masterOfHouse.model.Device;

public interface DeviceRepository extends JpaRepository<Device, Integer>,
		AdvancedDeviceRepository {

	@Query("select p from Device p where p.building = :building")
	public List<Device> getDevicesFromBuilding(
			@Param("building") Building buildingId);

	@Query("select p from Device p where p.building = :building and p.isConfirmed = :confirmed")
	public List<Device> getDevicesFromBuilding(
			@Param("building") Building buildingId,
			@Param("confirmed") Boolean isConfirmed);

}
