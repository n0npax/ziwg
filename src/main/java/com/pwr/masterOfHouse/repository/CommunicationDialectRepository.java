package com.pwr.masterOfHouse.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pwr.masterOfHouse.model.CommunicationDialect;

public interface CommunicationDialectRepository extends JpaRepository<CommunicationDialect, Integer> {

    @Query("select p from CommunicationDialect p where propertyLocation=?1")
    public List<CommunicationDialect> getCommunicationDialectsFromPropertyLocation(Integer propertyLocationId);

    @Query("select p from CommunicationDialect p where propertyLocation=?1 and isConfirmed=?2")
    public List<CommunicationDialect> getCommunicationDialectsFromPropertyLocation(Integer propertyLocationId,
                                                     Boolean isConfirmed);

}
