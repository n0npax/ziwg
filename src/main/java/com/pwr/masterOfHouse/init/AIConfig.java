package com.pwr.masterOfHouse.init;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.pwr.masterOfHouse.ai.DummyAIService;
import com.pwr.masterOfHouse.ai.HistoryComplementer;

@Configuration
public class AIConfig {

	@Bean
	public DummyAIService aiService() {
		return new DummyAIService();
	}
	
	@Bean
	public HistoryComplementer historyComplementer() {
		return new HistoryComplementer();
	}
	
}
