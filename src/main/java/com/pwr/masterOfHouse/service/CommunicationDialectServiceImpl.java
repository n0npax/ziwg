package com.pwr.masterOfHouse.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pwr.masterOfHouse.exception.CommunicationDialectNotFoundException;
import com.pwr.masterOfHouse.model.CommunicationDialect;
import com.pwr.masterOfHouse.repository.CommunicationDialectRepository;

@Service
@Transactional
public class CommunicationDialectServiceImpl implements CommunicationDialectService {

    @Autowired
    private CommunicationDialectRepository communicationDialectRepository;

    @Override
    public CommunicationDialect create(CommunicationDialect sp) {
        @SuppressWarnings("unchecked")
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
                .getContext().getAuthentication().getAuthorities();
        for (SimpleGrantedAuthority auth : authorities) {
            if (auth.getAuthority().equalsIgnoreCase("ROLE_ADMIN")) {
                sp.setIsConfirmed(true);
                break;
            } else {
                sp.setIsConfirmed(false);
            }
        }
        return communicationDialectRepository.save(sp);
    }

    @Override
    public CommunicationDialect get(Integer id) {
        return communicationDialectRepository.findOne(id);
    }

    @Override
    public List<CommunicationDialect> getAll() {
        return communicationDialectRepository.findAll();
    }

    @Override
    public CommunicationDialect update(CommunicationDialect sp) throws CommunicationDialectNotFoundException {
        CommunicationDialect communicationDialect = get(sp.getId());
        if (communicationDialect == null)
            throw new CommunicationDialectNotFoundException(sp.getId().toString());
        @SuppressWarnings("unchecked")
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
                .getContext().getAuthentication().getAuthorities();
        for (SimpleGrantedAuthority auth : authorities) {
            if (auth.getAuthority().equalsIgnoreCase("ROLE_ADMIN")) {
                sp.setIsConfirmed(true);
                break;
            } else {
                sp.setIsConfirmed(false);
            }
        }
        return communicationDialect.update(sp);
    }

    @Override
    public CommunicationDialect delete(Integer id) throws CommunicationDialectNotFoundException {
        CommunicationDialect communicationDialect = get(id);
        if (communicationDialect == null)
            throw new CommunicationDialectNotFoundException(id.toString());
        communicationDialectRepository.delete(id);
        return communicationDialect;
    }

    @Override
    public List<CommunicationDialect> getCommunicationDialectsFromPropertyLocation(Integer propertyLocationId) {
        return communicationDialectRepository.getCommunicationDialectsFromPropertyLocation(propertyLocationId);
    }

    @Override
    public List<CommunicationDialect> getCommunicationDialectsFromPropertyLocation(Integer propertyLocationId,
                                                     Boolean isConfirmed) {
        return communicationDialectRepository.getCommunicationDialectsFromPropertyLocation(propertyLocationId,
                isConfirmed);
    }

}
