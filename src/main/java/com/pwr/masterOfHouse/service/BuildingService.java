package com.pwr.masterOfHouse.service;

import java.util.List;

import com.pwr.masterOfHouse.exception.BuildingNotFoundException;
import com.pwr.masterOfHouse.model.Building;

public interface BuildingService {

    public Building create(Building sp);

    public Building get(Integer id);

    public List<Building> getAll();

    public List<Building> getBuildingsFromPropertyLocation(Integer propertyLocationId);

    public List<Building> getBuildingsFromPropertyLocation(Integer propertyLocationId,
                                                           Boolean isConfirmed);

    public Building update(Building sp) throws BuildingNotFoundException;

    public Building delete(Integer id) throws BuildingNotFoundException;

}
