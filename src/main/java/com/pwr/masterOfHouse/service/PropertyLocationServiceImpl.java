package com.pwr.masterOfHouse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pwr.masterOfHouse.exception.PropertyLocationNotFoundException;
import com.pwr.masterOfHouse.model.Building;
import com.pwr.masterOfHouse.model.CommunicationDialect;
import com.pwr.masterOfHouse.model.PropertyLocation;
import com.pwr.masterOfHouse.repository.BuildingRepository;
import com.pwr.masterOfHouse.repository.CommunicationDialectRepository;
import com.pwr.masterOfHouse.repository.PropertyLocationRepository;

@Service
@Transactional
public class PropertyLocationServiceImpl implements PropertyLocationService {

    @Autowired
    private PropertyLocationRepository propertyLocationRepository;

    @Autowired
    private BuildingRepository buildingRepository;

    @Autowired
    private CommunicationDialectRepository communicationDialectRepository;

    @Override
    public PropertyLocation create(PropertyLocation sp) {
        return propertyLocationRepository.save(sp);
    }

    @Override
    public PropertyLocation get(Integer id) {
        return propertyLocationRepository.findOne(id);
    }

    @Override
    public List<PropertyLocation> getAll() {
        return propertyLocationRepository.findAll();
    }

    @Override
    public PropertyLocation update(PropertyLocation sp) throws PropertyLocationNotFoundException {
        PropertyLocation propertyLocation = get(sp.getId());
        if (propertyLocation == null)
            throw new PropertyLocationNotFoundException(sp.getId().toString());
        propertyLocation.update(sp);
        return propertyLocation;
    }

    @Override
    public PropertyLocation delete(Integer id) throws PropertyLocationNotFoundException {
        PropertyLocation propertyLocation = get(id);
        if (propertyLocation == null)
            throw new PropertyLocationNotFoundException(id.toString());

        /*try {
            List<Building> buildingsToRemove = buildingRepository
                    .getBuildingsFromPropertyLocation(id);
            for (Building b : buildingsToRemove) {
                buildingRepository.delete(b);
            }
        }catch (Exception e)
        {}
        try {
            List<CommunicationDialect> communicationDialectsToRemove = communicationDialectRepository
                    .getCommunicationDialectsFromPropertyLocation(id);
            for (CommunicationDialect b : communicationDialectsToRemove) {
                communicationDialectRepository.delete(b);
            }
        }
        catch (Exception e){}*/
        propertyLocationRepository.delete(id);
        return propertyLocation;
    }

}
