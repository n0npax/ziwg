package com.pwr.masterOfHouse.service;

import java.util.List;

import com.pwr.masterOfHouse.exception.CommunicationDialectNotFoundException;
import com.pwr.masterOfHouse.model.CommunicationDialect;

public interface CommunicationDialectService {

    public CommunicationDialect get(Integer id);

    public List<CommunicationDialect> getAll();

    public List<CommunicationDialect> getCommunicationDialectsFromPropertyLocation(Integer propertyLocationId);

    public List<CommunicationDialect> getCommunicationDialectsFromPropertyLocation(Integer propertyLocationId,
                                                     Boolean isConfirmed);

    public CommunicationDialect update(CommunicationDialect sp) throws CommunicationDialectNotFoundException;

    public CommunicationDialect delete(Integer id) throws CommunicationDialectNotFoundException;

    public CommunicationDialect create(CommunicationDialect sp);

}
