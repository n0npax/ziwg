package com.pwr.masterOfHouse.service;

import java.util.List;

import com.pwr.masterOfHouse.exception.PropertyLocationNotFoundException;
import com.pwr.masterOfHouse.model.PropertyLocation;

public interface PropertyLocationService {

    public PropertyLocation create(PropertyLocation sp);

    public PropertyLocation get(Integer id);

    public List<PropertyLocation> getAll();

    public PropertyLocation update(PropertyLocation sp) throws PropertyLocationNotFoundException;

    public PropertyLocation delete(Integer id) throws PropertyLocationNotFoundException;

}
