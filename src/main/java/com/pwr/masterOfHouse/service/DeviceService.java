package com.pwr.masterOfHouse.service;

import java.util.List;

import com.pwr.masterOfHouse.exception.DeviceNotFoundException;
import com.pwr.masterOfHouse.model.Building;
import com.pwr.masterOfHouse.model.Device;

public interface DeviceService {

	public Device get(Integer id);

	public List<Device> getAll();

	public List<Device> getDevicesFromBuilding(Building building);

	public List<Device> getDevicesFromBuilding(Building buildings,
			Boolean isConfirmed);

	public Device update(Device sp) throws DeviceNotFoundException;

	public Device delete(Integer id) throws DeviceNotFoundException;

	public Device create(Device sp);

}
