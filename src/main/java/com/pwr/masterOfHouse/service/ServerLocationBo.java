package com.pwr.masterOfHouse.service;

import com.pwr.masterOfHouse.model.ServerLocation;

public interface ServerLocationBo {

    ServerLocation getLocation(String ipAddress);

}