package com.pwr.masterOfHouse.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pwr.masterOfHouse.exception.BuildingNotFoundException;
import com.pwr.masterOfHouse.model.Building;
import com.pwr.masterOfHouse.repository.BuildingRepository;

@Service
@Transactional
public class BuildingServiceImpl implements BuildingService {

    @Autowired
    private BuildingRepository buildingRepository;

    @Override
    public Building create(Building sp) {
        @SuppressWarnings("unchecked")
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
                .getContext().getAuthentication().getAuthorities();
        for (SimpleGrantedAuthority auth : authorities) {
            if (auth.getAuthority().equalsIgnoreCase("ROLE_ADMIN")) {
                sp.setIsConfirmed(true);
                break;
            } else {
                sp.setIsConfirmed(false);
            }
        }
        return buildingRepository.save(sp);
    }

    @Override
    public Building get(Integer id) {
        return buildingRepository.findOne(id);
    }

    @Override
    public List<Building> getAll() {
        return buildingRepository.findAll();
    }

    @Override
    public Building update(Building sp) throws BuildingNotFoundException {
        Building building = get(sp.getId());
        if (building == null)
            throw new BuildingNotFoundException(sp.getId().toString());
        @SuppressWarnings("unchecked")
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
                .getContext().getAuthentication().getAuthorities();
        for (SimpleGrantedAuthority auth : authorities) {
            if (auth.getAuthority().equalsIgnoreCase("ROLE_ADMIN")) {
                sp.setIsConfirmed(true);
                break;
            } else {
                sp.setIsConfirmed(false);
            }
        }
        return building.update(sp);
    }

    @Override
    public Building delete(Integer id) throws BuildingNotFoundException {
        Building building = get(id);
        if (building == null)
            throw new BuildingNotFoundException(id.toString());
        buildingRepository.delete(id);
        return building;
    }

    @Override
    public List<Building> getBuildingsFromPropertyLocation(Integer propertyLocationId) {
        return buildingRepository.getBuildingsFromPropertyLocation(propertyLocationId);
    }

    @Override
    public List<Building> getBuildingsFromPropertyLocation(Integer propertyLocationId,
                                                           Boolean isConfirmed) {
        return buildingRepository.getBuildingsFromPropertyLocation(propertyLocationId,
                isConfirmed);
    }

}
