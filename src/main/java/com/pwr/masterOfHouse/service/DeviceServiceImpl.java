package com.pwr.masterOfHouse.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pwr.masterOfHouse.exception.DeviceNotFoundException;
import com.pwr.masterOfHouse.model.Building;
import com.pwr.masterOfHouse.model.Device;
import com.pwr.masterOfHouse.repository.DeviceRepository;

@Service
@Transactional
public class DeviceServiceImpl implements DeviceService {

	@Autowired
	private DeviceRepository deviceRepository;

	@Override
	public Device create(Device sp) {
		@SuppressWarnings("unchecked")
		Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
				.getContext().getAuthentication().getAuthorities();
		for (SimpleGrantedAuthority auth : authorities) {
			if (auth.getAuthority().equalsIgnoreCase("ROLE_ADMIN")) {
				sp.setIsConfirmed(true);
				break;
			} else {
				sp.setIsConfirmed(false);
			}
		}
		return deviceRepository.save(sp);
	}

	@Override
	public Device get(Integer id) {
		return deviceRepository.findOne(id);
	}

	@Override
	public List<Device> getAll() {
		return deviceRepository.findAll();
	}

	@Override
	public Device update(Device sp) throws DeviceNotFoundException {
		Device device = get(sp.getId());
		if (device == null)
			throw new DeviceNotFoundException(sp.getId().toString());
		@SuppressWarnings("unchecked")
		Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
				.getContext().getAuthentication().getAuthorities();
		for (SimpleGrantedAuthority auth : authorities) {
			if (auth.getAuthority().equalsIgnoreCase("ROLE_ADMIN")) {
				sp.setIsConfirmed(true);
				break;
			} else {
				sp.setIsConfirmed(false);
			}
		}
		return device.update(sp);
	}

	@Override
	public Device delete(Integer id) throws DeviceNotFoundException {
		Device device = get(id);
		if (device == null)
			throw new DeviceNotFoundException(id.toString());
		deviceRepository.delete(id);
		return device;
	}

	@Override
	public List<Device> getDevicesFromBuilding(Building building) {
		return deviceRepository.getDevicesFromBuilding(building);
	}

	@Override
	public List<Device> getDevicesFromBuilding(Building building,
			Boolean isConfirmed) {
		return deviceRepository.getDevicesFromBuilding(building, isConfirmed);
	}

}
