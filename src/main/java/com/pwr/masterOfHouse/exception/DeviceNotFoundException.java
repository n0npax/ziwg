package com.pwr.masterOfHouse.exception;

public class DeviceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -2859292084648724408L;

    public DeviceNotFoundException(String deviceId) {
        super("Device not found with id: " + deviceId);
    }

}
