package com.pwr.masterOfHouse.exception;

public class BuildingNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -2859292084648724401L;

    public BuildingNotFoundException(String buildingId) {
        super("Building not found with id: " + buildingId);
    }

}
