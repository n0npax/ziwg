package com.pwr.masterOfHouse.exception;

public class PropertyLocationNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -2859292084648724403L;

    public PropertyLocationNotFoundException(String buildingId) {
        super("PropertyLocation not found with id: " + buildingId);
    }

}
