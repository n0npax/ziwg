package com.pwr.masterOfHouse.exception;

public class CommunicationDialectNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -2859292084648724408L;

    public CommunicationDialectNotFoundException(String communicationDialectId) {
        super("CommunicationDialect not found with id: " + communicationDialectId);
    }

}
