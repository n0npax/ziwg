package com.pwr.masterOfHouse.ai;

import com.pwr.masterOfHouse.model.Building;

public interface TemperatureManager {

	public void setTemperatureForBuilding(Building building, int temp);

}
