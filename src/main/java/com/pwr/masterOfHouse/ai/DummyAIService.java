package com.pwr.masterOfHouse.ai;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.pwr.masterOfHouse.model.Building;
import com.pwr.masterOfHouse.model.TemperatureControllingDevice;
import com.pwr.masterOfHouse.repository.DeviceRepository;
import com.pwr.masterOfHouse.repository.DeviceValuesRepository;

public class DummyAIService implements TemperatureManager {

	@Autowired
	DeviceRepository devicesRepo;
	@Autowired
	DeviceValuesRepository history;

	@Override
	public void setTemperatureForBuilding(Building building, int temperature) {
		List<TemperatureControllingDevice> devices = devicesRepo
				.getDevicesForTemperatureControlling(building);
		double averageTemperature = 0;
		int count = 0;
		for (TemperatureControllingDevice device : devices) {
			if (device.getEditable() == false) {
				double deviceTemperature = device.getTemperature();
				if (deviceTemperature > -10000) {
					averageTemperature += deviceTemperature;
					count++;
				}
			}
		}
		if (count > 0)
			averageTemperature /= count;
		for (TemperatureControllingDevice device : devices) {
			if (device.getEditable() == true) {
				device.setTemperature(temperature, (int) averageTemperature);
				devicesRepo.save(device);
			}
		}
	}
}
