package com.pwr.masterOfHouse.ai;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.pwr.masterOfHouse.model.Device;
import com.pwr.masterOfHouse.model.DeviceValues;
import com.pwr.masterOfHouse.repository.DeviceValuesRepository;

public class HistoryComplementer {

	@Autowired
	DeviceValuesRepository repo;

	public void persistSetByUser(String devValues, Device dev) {
		DeviceValues values = new DeviceValues();
		values.setDate(new Date());
		values.setDevice(dev);
		values.setSetByUser(true);
		values.setValues(devValues);
		repo.save(values);
	}

	public void persistReadFromDevice(String devValues, Device dev) {
		DeviceValues values = new DeviceValues();
		values.setDate(new Date());
		values.setDevice(dev);
		values.setSetByUser(false);
		values.setValues(devValues);
		repo.save(values);
	}

}
