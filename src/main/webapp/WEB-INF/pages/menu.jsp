<div id="header" class="header">
</div>

<% String url_page = request.getParameter("page"); %>

<div id="menu_container">
    <div id="cssmenu">
        <ul>
            <%--<li
                    <% if ("home".equals(url_page)){ %>
                    class="active"
                    <% } %>
                    >
                <a href="${pageContext.request.contextPath}/index.html?page=home"
                   title="Home"><span><spring:message code="menu.Home"/></span></a>
            </li>--%>

            <li class="has-sub <% if ("devices".equals(url_page) || "createDevice".equals(url_page)){ %> active<% } %>">
                <a <%--href="${pageContext.request.contextPath}/index.html?page=home"--%>
                        title="Devices"><span><spring:message code="menu.devices"/></span></a>

                <ul>
                    <li
                            <% if ("devices".equals(url_page)){ %>
                            class="active"
                            <% } %>
                            >
                        <a href="${pageContext.request.contextPath}/devices.html?page=devices"
                           title="Show devices"><span><spring:message code="menu.showDev"/></span></a>
                    </li>

                    <li class="last<% if ("createDevice".equals(url_page)){ %> active<% } %>">
                        <a href="${pageContext.request.contextPath}/devices/create.html?page=createDevice"
                           title="Create Device"><span><spring:message code="menu.createDev"/></span></a>
                    </li>
                </ul>
            </li>

            <li class="has-sub <% if ("locations".equals(url_page) || "createLocation".equals(url_page)){ %> active<% } %>">
                <a <%--href="${pageContext.request.contextPath}/index.html?page=home"--%>
                        title="Locations"><span><spring:message code="menu.locations"/></span></a>

                <ul>
                    <li
                            <% if ("locations".equals(url_page)){ %>
                            class="active"
                            <% } %>
                            >
                        <a href="${pageContext.request.contextPath}/propertyLocations.html?page=locations"
                           title="Show locations"><span><spring:message code="menu.showLoc"/></span></a>
                    </li>

                    <li class="last<% if ("createLocation".equals(url_page)){ %> active<% } %>">
                        <a href="${pageContext.request.contextPath}/propertyLocations/create.html?page=createLocation"
                           title="Create location"><span><spring:message code="menu.createLoc"/></span></a>
                    </li>
                </ul>
            </li>

            <li class="has-sub<% if ("buildings".equals(url_page) || "createBuilding".equals(url_page)){ %> active<% } %>">
                <a <%--href="${pageContext.request.contextPath}/index.html?page=home"--%>
                   title="Buildings"><span><spring:message code="menu.buildings"/></span></a>

                <ul>
                    <li
                            <% if ("buildings".equals(url_page)){ %>
                            class="active"
                            <% } %>
                            >
                        <a href="${pageContext.request.contextPath}/buildings.html?page=buildings"
                           title="Show Buildings"><span><spring:message code="menu.showBuild"/></span></a>
                    </li>

                    <li class="last<% if ("createBuilding".equals(url_page)){ %> active<% } %>">
                        <a href="${pageContext.request.contextPath}/buildings/create.html?page=createBuilding"
                           title="Create building"><span><spring:message code="menu.createBuild"/></span></a>
                    </li>
                </ul>
            </li>

            <li
                    class="last <% if ("logout".equals(url_page)){ %> active<% } %>">
                <a href="${pageContext.request.contextPath}/j_spring_security_logout?page=logout"
                   title="Logout"><span><spring:message code="menu.logout"/></span></a>
            </li>
        </ul>
    </div>
    <%@include file="lang_menu.jsp" %>
</div>