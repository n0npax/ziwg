<?xml version="1.0" encoding="UTF-8" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="index.title"/></title>
    <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet"/>
</head>
<body>
 <div id="header" class="header2">
 </div>

<c:if test="${'fail' eq param.auth}">
    <div style="color:#ff239f">
        Login Failed!!!<br />
        Reason : ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
    </div>
</c:if>
<div class="container">
    <div class="centering-wrapper">
        <div class="centered-element">
            <form action="${pageContext.request.contextPath}/login" method="post">
                <table class="loginTable">
                    <tr>
                        <td><spring:message code="login.username"/>:</td>
                        <td><input type='text' name="username" value='n0npax'/></td>
                    </tr>
                    <tr>
                        <td><spring:message code="login.password"/>:</td>
                        <td><input type='password' name="password" value='pass'/></td>
                    </tr>
                    <tr>
                        <td colspan='2'><input name="submit" type="submit" value="Login"/></td>
                    </tr>
                </table>
                <%@include file="lang_menu.jsp" %>
            </form>
        </div>
    </div>
</div>

</body>
</html>