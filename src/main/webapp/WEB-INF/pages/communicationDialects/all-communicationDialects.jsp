<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="communicationDialect.show_all_title"/></title>
    <link href="resources/css/main.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            var deleteLink = $("a:contains('Delete')");

            $(deleteLink).click(function (event) {

                $.ajax({
                    url: $(event.target).attr("href"),
                    type: "DELETE",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },

                    success: function (buildings) {
                        var respContent = "";
                        var rowToDelete = $(event.target).closest("tr");

                        rowToDelete.remove();

                        respContent += "<span class='success'>building was deleted: [";
                        respContent += building.tag + " : ";
                        respContent += building.name + " : ";
                        respContent += building.clat + " : ";
                        respContent += building.clong + " : ";
                        respContent += building.open_hours + " : ";
                        respContent += building.address + " : ";
                        respContent += building.floor + " : ";
                        respContent += building.room + " : ";
                        respContent += building.description + " : ";
                        respContent += building.phone + " : ";
                        respContent += building.mail + " : ";
                        respContent += building.www + " : ";
                        respContent += building.price + "]</span>";

                        $("#sCommunicationDialectFromResponse").html(respContent);
                    }
                });

                event.preventDefault();
            });

        });
    </script>

</head>
<body>

<div id="all" class="all">
    <%@include file="../menu.jsp" %>

    <div id="container">
        <h1><spring:message code="communicationDialect.show_all_title"/></h1>

        <div>
           <%-- <p><spring:message code="communicationDialect.as_title"/></p>--%>

            <div id="sCommunicationDialectFromResponse"></div>
        </div>


        <table <%--border="1px" cellpadding="2" cellspacing="2" width="800"--%>>
            <thead>
            <tr>
                <th><spring:message code="welcome.tag"/></th>
                <th><spring:message code="welcome.name"/></th>
                <th><spring:message code="welcome.clat"/></th>
                <th><spring:message code="welcome.clong"/></th>
                <th><spring:message code="welcome.open_hours"/></th>
                <th><spring:message code="welcome.address"/></th>
                <th><spring:message code="welcome.department"/></th>
                <th><spring:message code="welcome.directions"/></th>
                <th><spring:message code="welcome.photo"/></th>
                <th><spring:message code="welcome.description"/></th>
                <th><spring:message code="welcome.phone"/></th>
                <th><spring:message code="welcome.mail"/></th>
                <th><spring:message code="welcome.www"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="sCommunicationDialect" items="${communicationDialects}">
                <tr>

                    <td>${sCommunicationDialect.tag}</td>
                    <td>${sCommunicationDialect.name}</td>
                    <td>${sCommunicationDialect.clat} </td>
                    <td>${sCommunicationDialect.clong} </td>
                    <td>${sCommunicationDialect.open_hours} </td>
                    <td>${sCommunicationDialect.address} </td>
                    <td>${sCommunicationDialect.floor} </td>
                    <td>${sCommunicationDialect.floor} </td>
                    <td>${sCommunicationDialect.room} </td>
                    <td>${sCommunicationDialect.description}</td>
                    <td>${sCommunicationDialect.phone} </td>
                    <td>${sCommunicationDialect.mail} </td>
                    <td>${sCommunicationDialect.www}</td>
                    <td>
                        <a name="edit${sCommunicationDialect.id}" href="${pageContext.request.contextPath}/communicationDialects/edit/${sCommunicationDialect.id}.html">Edit</a><br/>
                        <a name="xml${sCommunicationDialect.id}" href="${pageContext.request.contextPath}/communicationDialects/xml/${sCommunicationDialect.id}.xml">To Xml</a><br/>
                        <a name="del${sCommunicationDialect.id}" href="${pageContext.request.contextPath}/communicationDialects/delete/${sCommunicationDialect.id}.json">Delete</a><br/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

    </div>
</div>
</body>
</html>