<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
    <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet">



    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="device.add_new_title"/></title>
    <%--<link href="../resources/css/main.css" rel="stylesheet" type="text/css"/>--%>
    <link href="/masterOfHouse/resources/mytheme/css/main.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#newDeviceFrom').submit(function (event) {

                var tag = $('#tag').val();
                var name = $('#name').val();
                var communicationDialect = $('#communicationDialect').val();
                var editable = $('input:checkbox[name=editableCheckbox]').is(':checked');
                var description = $('#description').val();
                var devValues = $('#devValues').val();
                var xmlDevDesc = $('#xmlDevDesc').val();
                var www = $('#www').val();
                var building = $('#building').val();
                var json = {
                    "tag": tag,
                    "name": name,


                    "editable": editable,
                    "description": description,
                    "devValues": devValues,
                    "communicationDialect":communicationDialect,
                    "xmlDevDesc": xmlDevDesc,
                    "www": www,
                    "building": building
                };

                $.ajax({
                    url: $("#newDeviceFrom").attr("action"),
                    data: JSON.stringify(json),
                    type: "POST",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (device) {
                        var respContent = "";

                        respContent += "<span class='success'>Devices was created: [";

                        respContent += device.tag + " : ";
                        respContent += device.name + " : ";


                        respContent += device.editable + " : ";
                        respContent += device.description + " : ";
                        respContent += device.devValues + " : ";
                        respContent += device.xmlDevDesc + " : ";
                        respContent += device.www + " : ";
                        respContent += device.communicationDialect + " : ";
                        respContent += device.building + "]</span>";



                        $("#sDevvaluesFromResponse").html(respContent);
                    }
                });

                event.preventDefault();
            });

        });
    </script>


</head>


<body>

<div id="all" class="all">
    <%@include file="../menu.jsp" %>

    <div id="container">
        <h1><spring:message code="device.edit_titile"/></h1>

        <div>
            <div id="sDevvaluesFromResponse"></div>
        </div>
        <form:form id="newDeviceFrom"
                   action="${pageContext.request.contextPath}/devices/edit/${device.id}.json"
                   commandName="device">
        <table>
            <tr>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td>building:</td>
                            <td>
                                <form:select path="building">
                                    <c:forEach var="u" items="${univ}">
                                        <form:option value="${u.id}"> ${u.name} </form:option>
                                    </c:forEach>
                                </form:select>
                            </td>
                        </tr>

                        <tr>
                            <td>comd:</td>
                            <td>
                                <form:select path="communicationDialect">
                                    <c:forEach var="u" items="${comd}">
                                        <form:option value="${u.id}"> ${u.name} </form:option>
                                    </c:forEach>
                                </form:select>
                            </td>
                        </tr>

                        <tr>
                            <td><spring:message code="welcome.tag"/></td>
                            <td><form:input path="tag"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="welcome.name"/></td>
                            <td><form:input path="name"/></td>
                        </tr>


                        <tr>
                            <td><spring:message code="welcome.editable"/>:</td>
                            <td> editable <input type="checkbox" name="editableCheckbox"></input></td>
                        </tr>

                        <tr>
                            <td><spring:message code="welcome.description"/>:</td>
                            <td><form:textarea rows="5" cols="19" path="description"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="welcome.devValues"/></td>
                            <td><form:input path="devValues"/></td>
                        </tr>

                        <tr>
                            <td><spring:message code="welcome.xmlDevDesc"/></td>
                            <td><form:input path="xmlDevDesc"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="welcome.www"/>:</td>
                            <td><form:input path="www"/></td>
                        </tr>


                        <tr>
                            <td><input type="submit" value="    OK   "/></td>
                            <td></td>
                        </tr>

                        </tbody>
                    </table>
                    </form:form>
    </div>

</div>
</body>
</html>