<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="device.show_all_title"/></title>
    <link href="resources/css/main.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            var deleteLink = $("a:contains('Delete')");

            $(deleteLink).click(function (event) {

                $.ajax({
                    url: $(event.target).attr("href"),
                    type: "DELETE",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },

                    success: function (buildings) {
                        var respContent = "";
                        var rowToDelete = $(event.target).closest("tr");

                        rowToDelete.remove();

                        respContent += "<span class='success'>building was deleted: [";
                        respContent += building.building + " : ";
                        respContent += building.tag + " : ";
                        respContent += building.name + " : ";
                        respContent += building.clat + " : ";
                        respContent += building.clong + " : ";
                        respContent += building.open_hours + " : ";
                        respContent += building.address + " : ";
                        respContent += building.pbuilding + " : ";
                        respContent += building.floor + " : ";
                        respContent += building.editable + " : ";
                        respContent += building.description + " : ";
                        respContent += building.devValues + " : ";
                        respContent += building.xmlDevDesc + " : ";
                        respContent += building.www + " : ";
                        respContent += building.price + "]</span>";

                        $("#sDeviceFromResponse").html(respContent);
                    }
                });

                event.preventDefault();
            });

        });
    </script>

</head>
<body>

<div id="all" class="all">
    <%@include file="../menu.jsp" %>


    <div id="container">
        <h1><spring:message code="device.show_all_title"/></h1>

        <div>
            <p>

                <select name='role'>
                    <option value="${selected}" selected>${selected}</option>
                    <c:forEach items="${buildings}" var="role">
                        <c:if test="${role != selected}">
                            <option value="${role}">${role}</option>
                        </c:if>
                    </c:forEach>
                </select>
                <c:out value="${role}"/>

            </p>

            <div id="sDeviceFromResponse"></div>
        </div>
        <table <%--border="1px" cellpadding="2" cellspacing="2"--%>>
            <thead>
            <tr>
                <th><spring:message code="welcome.building"/></th>
                <th><spring:message code="welcome.tag"/></th>
                <th><spring:message code="welcome.name"/></th>
                <th><spring:message code="welcome.clat"/></th>
                <th><spring:message code="welcome.clong"/></th>
                <th><spring:message code="welcome.open_hours"/></th>
                <th><spring:message code="welcome.address"/></th>
                <th><spring:message code="welcome.department"/></th>
                <th><spring:message code="welcome.directions"/></th>
                <th><spring:message code="welcome.photo"/></th>
                <th><spring:message code="welcome.description"/></th>
                <th><spring:message code="welcome.devValues"/></th>
                <th><spring:message code="welcome.xmlDevDesc"/></th>
                <th><spring:message code="welcome.www"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="sDevice" items="${devices}">
                <tr>

                    <td>${sDevice.building}</td>
                    <td>${sDevice.tag}</td>
                    <td>${sDevice.name}</td>
                    <td>${sDevice.clat} </td>
                    <td>${sDevice.clong} </td>
                    <td>${sDevice.open_hours} </td>
                    <td>${sDevice.address} </td>
                    <td>${sDevice.floor} </td>
                    <td>${sDevice.floor} </td>
                    <td>${sDevice.editable} </td>
                    <td>${sDevice.description}</td>
                    <td>${sDevice.devValues} </td>
                    <td>${sDevice.xmlDevDesc} </td>
                    <td>${sDevice.www}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/devices/edit/${sDevice.id}.html">Edit</a><br/>
                        <a href="${pageContext.request.contextPath}/devices/xml/${sDevice.id}.xml">To Xml</a><br/>
                        <a href="${pageContext.request.contextPath}/devices/delete/${sDevice.id}.json">Delete</a><br/>
                        <a href="${pageContext.request.contextPath}/devices/confirm/${sDevice.id}">Confirm</a><br/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

    </div>
</div>
</body>
</html>