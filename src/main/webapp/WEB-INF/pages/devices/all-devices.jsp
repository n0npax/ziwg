<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="device.show_all_title"/></title>
    <link href="resources/css/main.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            var deleteLink = $("a:contains('Delete')");
            
            var aiOkButton = $("button:contains('OK')");

            $(deleteLink).click(function (event) {

                $.ajax({
                    url: $(event.target).attr("href"),
                    type: "DELETE",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },

                    success: function (buildings) {
                        var respContent = "";
                        var rowToDelete = $(event.target).closest("tr");

                        rowToDelete.remove();

                        respContent += "<span class='success'>building was deleted: [";
                        respContent += building.building + " : ";
                        respContent += building.tag + " : ";
                        respContent += building.name + " : ";


                        respContent += building.editable + " : ";
                        respContent += building.description + " : ";
                        respContent += building.devValues + " : ";
                        respContent += building.xmlDevDesc + " : ";
                        respContent += building.www + " : ";
                        respContent += building.price + "]</span>";

                        $("#sDeviceFromResponse").html(respContent);
                    }
                });

                event.preventDefault();
            });
            
            $(aiOkButton).click(function (event) {
            	var temperature = $('#buildingTemperature').val();
            	var building = $('#activeBuildingId').text();
            	
            	var json = {
            		"buildingTemperature": temperature,
            		"building": building
            	}
            	
            	$.ajax({
            		url: $(event.target).attr("href"),
                    type: "POST",
                    data: JSON.stringify(json),
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "text/html");
                        xhr.setRequestHeader("Content-Type", "text/html");
                    },
                    success: function (answer) {
                       $('#aiDiv').css("background-color","#33FF00");
                       var setColorBack = function() {
                    	   $('#aiDiv').css("background-color", "lightgray");
                       }
                       setTimeout(setColorBack, 1000);
                    }
            	});
            	
            	event.preventDefault();
            });

        });
    </script>

</head>
<body>

<div id="all" class="all">
    <%@include file="../menu.jsp" %>

    <div id="container">
        <h1><spring:message code="device.show_all_title"/></h1>

        <div>
            <div id="sDeviceFromResponse"></div>
        </div>

        <div id="choose">
            <c:forEach var="u" items="${univ}">
                <a href="${pageContext.request.contextPath}/devices/allDevicesFromBuilding/${u.id}?page=devices">
                    ${u.name}
                </a>
            </c:forEach>
        </div>

    </div>

    <%! int valOuter = 0; %>
    <%! int valInner; %>
    <%-- var valOuter = 0 --%>
    <c:forEach var="sDevice" items="${devices}">
        <div id="tiles">
            <a href = "javascript:void(0)"
               onclick = "document.getElementById('light<%=valOuter%>').style.display='block';
               document.getElementById('fade<%=valOuter%>').style.display='block'">${sDevice.name}</a>

            <div id="light<%=valOuter%>" class="white_content">
                <p align="right"><a href = "javascript:void(0)"
                   onclick = "document.getElementById('light<%=valOuter%>').style.display='none';
                   document.getElementById('fade<%=valOuter%>').style.display='none'" ><spring:message code="device.popup_close"/></a></p>
                <p><spring:message code="device.popup_text"/> ${sDevice.name}</p>
                <table>
                    <thead>
                    <tr>
                        <%--<th><spring:message code="welcome.building"/></th>--%>
                        <th><spring:message code="welcome.tag"/></th>
                        <th><spring:message code="welcome.name"/></th>
                        <th><spring:message code="welcome.editable"/></th>
                        <th><spring:message code="welcome.description"/></th>
                        <th><spring:message code="welcome.devValues"/></th>
                        <th><spring:message code="welcome.xmlDevDesc"/></th>
                        <th><spring:message code="welcome.www"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <%-- valInner = 0  --%>
                    <% valInner = 0; %>
                    <c:forEach var="sDevice" items="${devices}">

                        <%-- if:: valOuter == valInner --%>
                        <% if (valOuter == valInner) { %>
                        <tr>
                            <%--<td>${sDevice.building}</td>--%>
                            <td>${sDevice.tag}</td>
                            <td>${sDevice.name}</td>
                            <td>${sDevice.editable} </td>
                            <td>${sDevice.description}</td>
                            <td>${sDevice.getDevValues2()}</td>
                            <td>${sDevice.xmlDevDesc} </td>
                            <td>${sDevice.www}</td>
                            <td>
                                <a name="edit${sDevice.id}" href="${pageContext.request.contextPath}/devices/edit/${sDevice.id}.html">Edit</a><br/>
                                <a name="xml${sDevice.id}" href="${pageContext.request.contextPath}/devices/xml/${sDevice.id}.xml">To Xml</a><br/>
                                <a name="del${sDevice.id}" href="${pageContext.request.contextPath}/devices/delete/${sDevice.id}.json">Delete</a><br/>
                            </td>
                        </tr>
                        <% } %>
                        <%-- valInner++ --%>
                        <% valInner++; %>
                    </c:forEach>
                    </tbody>
                </table>

            </div>
                   <div id="fade<%=valOuter%>" class="black_overlay"></div>

        </div>
        <%-- valOuter++ --%>
        <% valOuter++; %>
    </c:forEach>
 	<div id="tiles" style="text-align:center">
  		<div id="aiDiv" style="margin: 10px 10px; margin-bottom: 0; padding-top: 20px; padding-bottom: 20px; background-color: lightgray">
  			<span style="color: blue; font-weight: bold">Building temperature:</span><br/>
  			<input type="text" id="buildingTemperature" style="width: 30px; margin-top: 10px"></input><br/>
  			<button style="margin-top: 10px" href="${pageContext.request.contextPath}/buildingTemperature">OK</button>
  			<span id="activeBuildingId" style="display: none">${activeBuilding}</span>
  		</div>
  	</div>

</div>
</body>
</html>