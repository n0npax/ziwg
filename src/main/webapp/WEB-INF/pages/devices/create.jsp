<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<link href="<c:url value="/resources/mytheme/css/main.css" />"
	rel="stylesheet">

	<script type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzvmWZOjMUJd4F5RF2x4tQhvgoHZ3V99k&sensor=false">
    </script>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><spring:message code="device.add_new_title" /></title>
	<link href="../resources/css/main.css" rel="stylesheet" type="text/css" />

	<%-- <script
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"
		type="text/javascript"></script> --%>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

	<script type="text/javascript">


        $(document).ready(function () {
        	
        	$('#newDeviceFrom').submit(function (event) {

                var tag = $('#tag').val();
                var name = $('#name').val();
                var communicationDialect = $('#communicationDialect').val();
                var editable = $('input:checkbox[name=editableCheckbox]').is(':checked');
                var description = $('#description').val();
                var devValues = $('#devValues').val();
                var xmlDevDesc = $('#xmlDevDesc').val();
                var www = $('#www').val();
                var building = $('#building').val();
                var json = {
                    "tag": tag,
                    "name": name,
                    "editable": editable,
                    "description": description,
                    "devValues": devValues,
                    "xmlDevDesc": xmlDevDesc,
                    "www": www,
                    "temperatureControlling": false,
                    "communicationDialect":communicationDialect,
                    "building": building,
                    "isConfirmed": false
                };
                
                $.ajax({
                    url: $("#newDeviceFrom").attr("action"),
                    data: JSON.stringify(json),
                    type: "POST",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (device) {

                        var respContent = "";

						respContent += "<span class='success'>Device was created: [";
                        respContent += device.tag + " : ";
                        respContent += device.name + " : ";
                        respContent += device.editable + " : ";
                        respContent += device.description + " : ";
                        respContent += device.devValues + " : ";
                        respContent += device.xmlDevDesc + " : ";
                        respContent += device.www + " : ";
                        respContent += device.communicationDialect + " : ";
                        respContent += device.building + "]</span>";

                        $("#transmissionFromResponse").html(respContent);
						$("#transmissionFromResponse").dialog({
							modal: true,
							buttons: {
								Ok: function() {
									$( this ).dialog( "close" );
								}
							}
						});
                    }
                });

                event.preventDefault();
            });

        });
    </script>
</head>


<body>

	<div id="all" class="all">
		<%@include file="../menu.jsp"%>


		<div id="container">
			<h1>
				<spring:message code="device.add_new_title" />
			</h1>

			<div>
				<div id="transmissionFromResponse"></div>
			</div>
			<form:form id="newDeviceFrom"
				action="${pageContext.request.contextPath}/devices/create.json"
				commandName="device">

				<table>
					<tr>
						<td>
							<table>
								<tbody>
									<tr>
										<td>building:</td>
										<td><form:select path="building">
												<c:forEach var="u" items="${univ}">
													<form:option value="${u.id}"> ${u.name} </form:option>
												</c:forEach>
											</form:select></td>
									</tr>

									<tr>
										<td>comd:</td>
										<td><form:select path="communicationDialect">
												<c:forEach var="u" items="${comd}">
													<form:option value="${u.id}"> ${u.name} </form:option>
												</c:forEach>
											</form:select></td>
									</tr>

									<tr>
										<td><spring:message code="welcome.tag" /></td>
										<td><form:input path="tag" /></td>
									</tr>
									<tr>
										<td><spring:message code="welcome.name" /></td>
										<td><form:input path="name" /></td>
									</tr>


									<tr>
										<td><spring:message code="welcome.editable" />:</td>
										<td>editable <input type="checkbox"
											name="editableCheckbox"></input></td>
									</tr>

									<tr>
										<td><spring:message code="welcome.description" />:</td>
										<td><form:textarea rows="5" cols="19" path="description" /></td>
									</tr>
									<tr>
										<td><spring:message code="welcome.devValues" /></td>
										<td><form:input path="devValues" /></td>
									</tr>

									<tr>
										<td><spring:message code="welcome.xmlDevDesc" /></td>
										<td><form:input path="xmlDevDesc" /></td>
									</tr>
									<tr>
										<td><spring:message code="welcome.www" />:</td>
										<td><form:input path="www" /></td>
									</tr>


									<tr>
										<td><input type="submit" value="    OK   " /></td>
										<td></td>
									</tr>

								</tbody>
							</table> </form:form>
							</div>

							</div>
</body>
</html>