<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="building.show_all_title"/></title>
    <link href="resources/css/main.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            var deleteLink = $("a:contains('Delete')");

            $(deleteLink).click(function (event) {

                $.ajax({
                    url: $(event.target).attr("href"),
                    type: "DELETE",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },

                    success: function (buildings) {
                        var respContent = "";
                        var rowToDelete = $(event.target).closest("tr");

                        rowToDelete.remove();

                        respContent += "<span class='success'>building was deleted: [";
                        respContent += building.propertyLocation + " : ";
                        respContent += building.tag + " : ";
                        respContent += building.name + " : ";
                        respContent += building.clat + " : ";
                        respContent += building.clong + " : ";
                        respContent += building.open_hours + " : ";
                        respContent += building.address + " : ";
                        respContent += building.department + " : ";
                        respContent += building.directions + " : ";
                        respContent += building.photo + " : ";
                        respContent += building.description + " : ";
                        respContent += building.phone + " : ";
                        respContent += building.mail + " : ";
                        respContent += building.www + " : ";
                        respContent += building.price + "]</span>";

                        $("#sBuildingFromResponse").html(respContent);
                    }
                });

                event.preventDefault();
            });

        });
    </script>

</head>
<body>

<div id="all" class="all">
    <%@include file="../menu.jsp" %>

    <div id="container">
        <h1><spring:message code="building.show_all_title"/></h1>

        <div>
            <p><spring:message code="building.edit_title"/></p>

            <div id="sBuildingFromResponse"></div>
        </div>
        <table <%--border="1px" cellpadding="2" cellspacing="2"--%>>
            <thead>
            <tr>
                <th><spring:message code="welcome.propertyLocation"/></th>
                <th><spring:message code="welcome.tag"/></th>
                <th><spring:message code="welcome.name"/></th>
                <th><spring:message code="welcome.clat"/></th>
                <th><spring:message code="welcome.clong"/></th>
                <th><spring:message code="welcome.open_hours"/></th>
                <th><spring:message code="welcome.address"/></th>
                <th><spring:message code="welcome.department"/></th>
                <th><spring:message code="welcome.directions"/></th>
                <th><spring:message code="welcome.photo"/></th>
                <th><spring:message code="welcome.description"/></th>
                <th><spring:message code="welcome.phone"/></th>
                <th><spring:message code="welcome.mail"/></th>
                <th><spring:message code="welcome.www"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="sBuilding" items="${buildings}">
                <tr>

                    <td>${sBuilding.propertyLocation}</td>
                    <td>${sBuilding.tag}</td>
                    <td>${sBuilding.name}</td>
                    <td>${sBuilding.clat} </td>
                    <td>${sBuilding.clong} </td>
                    <td>${sBuilding.open_hours} </td>
                    <td>${sBuilding.address} </td>
                    <td>${sBuilding.department} </td>
                    <td>${sBuilding.directions} </td>
                    <td>${sBuilding.photo} </td>
                    <td>${sBuilding.description}</td>
                    <td>${sBuilding.phone} </td>
                    <td>${sBuilding.mail} </td>
                    <td>${sBuilding.www}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/buildings/edit/${sBuilding.id}.html">Edit</a><br/>
                        <a href="${pageContext.request.contextPath}/buildings/xml/${sBuilding.id}.xml">To Xml</a><br/>
                        <a href="${pageContext.request.contextPath}/buildings/delete/${sBuilding.id}.json">Delete</a><br/>
                        <a href="${pageContext.request.contextPath}/buildings/confirm/${sBuilding.id}">Confirm</a><br/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>