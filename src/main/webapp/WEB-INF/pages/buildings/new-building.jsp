<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet">

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzvmWZOjMUJd4F5RF2x4tQhvgoHZ3V99k&sensor=false">
    </script>

    <script type="text/javascript">
        var geocoder = new google.maps.Geocoder();
        var vlong = 0;
        var vlat = 0;
        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            });
        }
        function wyswietlMarker(address) {
            geocoder.geocode({'address': address},
                    function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            firstLoc = results[0].geometry.location;
                            map = new google.maps.Map(document.getElementById("mapCanvas"),
                                    {
                                        center: firstLoc,
                                        zoom: 15,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    });
                            var iconBase = 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/64/';
                            var marker = new google.maps.Marker({
                                position: firstLoc,
                                map: map,
                                draggable: true,
                                icon: iconBase + 'Map-Marker-Flag-2-Right-Azure-icon.png'

                            });

                            // Update current position info.
                            updateMarkerPosition(firstLoc);
                            geocodePosition(firstLoc);
                            marker.setAnimation(google.maps.Animation.BOUNCE);


                            google.maps.event.addListener(marker, 'drag', function () {
                                updateMarkerPosition(marker.getPosition());
                            });

                            google.maps.event.addListener(marker, 'dragend', function () {
                                geocodePosition(marker.getPosition());
                            });
                        }
                        else {
                            alert("Nie moge znaleźć adresu: " + address);
                        }
                    }
            );
        }

        function updateMarkerStatus(str) {
            document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng) {

            document.getElementById("clatv").value = latLng.lat();
            document.getElementById("clongv").value = latLng.lng();

            vlong = latLng.lng();
            vlat = latLng.lat();
        }

        function initialize() {

            var latLng = new google.maps.LatLng(51.108260661933365, 17.060721343994146);
            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 1,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var iconBase = 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/64/';
            var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                draggable: true,
                icon: iconBase + 'Map-Marker-Flag-2-Right-Azure-icon.png'

            });

            // Update current position info.
            updateMarkerPosition(latLng);
            geocodePosition(latLng);
            marker.setAnimation(google.maps.Animation.BOUNCE);


            google.maps.event.addListener(marker, 'drag', function () {
                updateMarkerPosition(marker.getPosition());
            });

            google.maps.event.addListener(marker, 'dragend', function () {
                geocodePosition(marker.getPosition());
            });
        }

        // Onload handler to fire off the app.
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="building.add_new_title"/></title>
    <link href="../resources/css/main.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#newBuildingForm').submit(function (event) {
            	event.preventDefault();

                var tag = $('#tag').val();
                var name = $('#name').val();
                var clong = vlong;
                var clat = vlat;
                var open_hours = $('#open_hours').val();
                var address = $('#address').val();
                var department = $('#department').val();
                var directions = $('#directions').val();
                var photo = $('#photo').val();
                var description = $('#description').val();
                var phone = $('#phone').val();
                var mail = $('#mail').val();
                var www = $('#www').val();
                var propertyLocation = $('#propertyLocation').val();
                var json = {
                    "tag": tag,
                    "name": name,
                    "clat": clat,
                    "clong": clong,
                    "open_hours": open_hours,
                    "address": address,
                    "department": department,
                    "directions": directions,
                    "photo": photo,
                    "description": description,
                    "phone": phone,
                    "mail": mail,
                    "www": www,
                    "propertyLocation": propertyLocation
                };
                
                $.ajax({
                    url: $("#newBuildingForm").attr("action"),
                    data: JSON.stringify(json),
                    type: "POST",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (building) {
                        var respContent = "";

                        respContent += "<span class='success'>Building was created: [";

                        respContent += building.tag + " : ";
                        respContent += building.name + " : ";
                        respContent += building.clat + " : ";
                        respContent += building.clong + " : ";
                        respContent += building.open_hours + " : ";
                        respContent += building.address + " : ";
                        respContent += building.department + " : ";
                        respContent += building.directions + " : ";
                        respContent += building.photo + " : ";
                        respContent += building.description + " : ";
                        respContent += building.phone + " : ";
                        respContent += building.mail + " : ";
                        respContent += building.www + " : ";
                        respContent += building.propertyLocation + "]</span>";

                        $("#transmissionFromResponse").html(respContent);
                    }
                });

                event.preventDefault();
            });

        });
    </script>


</head>
<body>

<div id="all" class="all">
    <%@include file="../menu.jsp" %>

    <div id="container">
        <h1><spring:message code="building.add_new_title"/></h1>

        <div>
            <div id="transmissionFromResponse"></div>
        </div>

        <table>
            <tr>
                <td>
                    <form:form id="newBuildingForm" action="${pageContext.request.contextPath}/buildings/create.json"
                               commandName="building">
                    <table>
                        <tbody>

                        <tr>
                            <td>Property Location:</td>
                            <td>
                                <form:select path="propertyLocation">
                                    <c:forEach var="u" items="${univ}">
                                        <form:option value="${u.id}">${u.name}</form:option>
                                    </c:forEach>
                                </form:select>
                            </td>
                        </tr>

                        <td>Tag:</td>
                        <td><form:input path="tag"/></td>
                        </tr>
                        <tr>
                            <td>Name:</td>
                            <td><form:input path="name"/></td>
                        </tr>

                        <tr>
                            <td>Open hours:</td>
                            <td><form:input path="open_hours"/></td>
                        </tr>

                        <tr>
                            <td>Address:</td>
                            <td><form:input path="address" onblur="wyswietlMarker($('#address').val());"/></td>
                        </tr>
                        <tr>
                            <td>Department:</td>
                            <td><form:input path="department"/></td>
                        </tr>


                        <tr>
                            <td>Directions:</td>
                            <td><form:input path="directions"/></td>
                        </tr>
                        <tr>
                            <td>Photo:</td>
                            <td><form:input path="photo"/></td>
                        </tr>


                        <tr>
                            <td>Description:</td>
                            <td><form:textarea rows="5" cols="19" path="description"/></td>
                        </tr>
                        <tr>
                            <td>Phone:</td>
                            <td><form:input path="phone"/></td>
                        </tr>

                        <tr>
                            <td>Mail:</td>
                            <td><form:input path="mail"/></td>
                        </tr>
                        <tr>
                            <td>WWW:</td>
                            <td><form:input path="www"/></td>
                        </tr>

                        <tr>
                            <!-- <td><form:input path="clong" id="clongv" type="hidden"/></td> -->
                            <!-- <td><form:input path="clat" id="clatv" type="hidden"/></td> -->
                        </tr>

                        <tr>
                            <td><input type="submit" value="    OK   "/></td>
                            <!-- <td></td> -->
                        </tr>

                        </tbody>
                    </table>
                    </form:form>
    </div>

</div>
</td>
<td>
    <style>
        #mapCanvas {
            width: 500px;
            height: 400px;
            float: left;
        }

        #infoPanel {
            float: left;
            margin-left: 10px;
        }

        #infoPanel div {
            margin-bottom: 5px;
        }
    </style>

    <div id="mapCanvas"></div>
    <div id="infoPanel">


    </div>
</td>
</tr></table>
</div>
</body>
</html>