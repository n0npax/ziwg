<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" %>
<div id="lang">
  <%--<p>Change language</p>--%>
    <a
        <% if ("pl".equals(RequestContextUtils.getLocale(request).toString())){ %>
          class="active"
        <% } %>
        href="?lang=pl">pl</a>
    <a
          <% if ("en".equals(RequestContextUtils.getLocale(request).toString())){ %>
          class="active"
          <% } %>
          href="?lang=en">en</a>

    <a
          <% if ("de".equals(RequestContextUtils.getLocale(request).toString())){ %>
          class="active"
          <% } %>
          href="?lang=de">de</a>
</div>