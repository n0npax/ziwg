<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet">

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzvmWZOjMUJd4F5RF2x4tQhvgoHZ3V99k&sensor=false">
    </script>

    <script type="text/javascript">
        var geocoder = new google.maps.Geocoder();
        var vlong = 0;
        var vlng = 0;
        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            });
        }
        function wyswietlMarker(address) {
            geocoder.geocode({'address': address},
                    function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            firstLoc = results[0].geometry.location;
                            map = new google.maps.Map(document.getElementById("mapCanvas"),
                                    {
                                        center: firstLoc,
                                        zoom: 15,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    });
                            var iconBase = 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/64/';
                            var marker = new google.maps.Marker({
                                position: firstLoc,
                                map: map,
                                draggable: true,
                                icon: iconBase + 'Map-Marker-Flag-2-Right-Azure-icon.png'

                            });

                            // Update current position info.
                            updateMarkerPosition(firstLoc);
                            geocodePosition(firstLoc);
                            marker.setAnimation(google.maps.Animation.BOUNCE);


                            google.maps.event.addListener(marker, 'drag', function () {
                                updateMarkerPosition(marker.getPosition());
                            });

                            google.maps.event.addListener(marker, 'dragend', function () {
                                geocodePosition(marker.getPosition());
                            });
                        }
                        else {
                            alert("Nie moge znaleźć adresu: " + address);
                        }
                    }
            );
        }

        function updateMarkerStatus(str) {
            document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng) {

            document.getElementById("clatv").value = latLng.lat();
            document.getElementById("clongv").value = latLng.lng();

            vlong = latLng.lng();
            vlat = latLng.lat();
        }

        function initialize() {

            var latLng = new google.maps.LatLng(51.108260661933365, 17.060721343994146);
            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 1,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var iconBase = 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/64/';
            var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                draggable: true,
                icon: iconBase + 'Map-Marker-Flag-2-Right-Azure-icon.png'

            });

            // Update current position info.
            updateMarkerPosition(latLng);
            geocodePosition(latLng);
            marker.setAnimation(google.maps.Animation.BOUNCE);


            google.maps.event.addListener(marker, 'drag', function () {
                updateMarkerPosition(marker.getPosition());
            });

            google.maps.event.addListener(marker, 'dragend', function () {
                geocodePosition(marker.getPosition());
            });
        }

        // Onload handler to fire off the app.
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="location.add_new_title"/></title>
    <link href="../resources/css/main.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#newPropertyLocationForm').submit(function (event) {

                var name = $('#name').val();
                var country = $('#country').val();
                var city = $('#city').val();
                var clong = vlong;
                var clat = vlat;
                var json = {"name": name, "clong": clong, "clat": clat, "country": country, "city": city};

                $.ajax({
                    url: $("#newPropertyLocationForm").attr("action"),
                    data: JSON.stringify(json),
                    type: "POST",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (propertyLocation) {
                        var respContent = "";

                        respContent += "<span class='success'>PropertyLocation was created: [";
                        respContent += propertyLocation.name + " : ";
                        respContent += propertyLocation.clong + " : ";
                        respContent += propertyLocation.clat + " : ";
                        respContent += propertyLocation.city + " : ";
                        respContent += propertyLocation.country + "]</span>";

                        $("#transmissionFromResponse").html(respContent);
                    }
                });

                event.preventDefault();
            });

        });
    </script>
</head>
<body>


<div id="all" class="all">
    <%@include file="../menu.jsp" %>

    <div id="container">
        <h1><spring:message code="location.edit_title"/></h1>

        <div>
            <div id="transmissionFromResponse"></div>
        </div>

        <table>
            <tr>
                <td>
                    <form:form id="newPropertyLocationForm"
                               action="${pageContext.request.contextPath}/propertyLocations/create.json"
                               commandName="propertyLocation">
                    <table>
                        <tbody>
                        <tr>
                            <td><spring:message code="welcome.country"/></td>
                            <td>
                                <form:select path="country">
                                    <form:option value="DE">DE</form:option>
                                    <form:option selected="selected" value="PL">PL</form:option>
                                    <form:option value="FR">FR</form:option>
                                    <form:option value="UK">UK</form:option>
                                    <form:option value="GB">GB</form:option>
                                    <form:option value="RU">RU</form:option>
                                    <form:option value="CZ">CZ</form:option>
                                    <form:option value="SL">SL</form:option>
                                    <form:option value="LT">LT</form:option>
                                    <form:option value="IT">IT</form:option>
                                    <form:option value="CR">CR</form:option>
                                    <form:option value="AU">AU</form:option>
                                    <form:option value="RU">RU</form:option>
                                    <form:option value="US">US</form:option>
                                    <form:option value="SW">SW</form:option>
                                    <form:option value="BL">BL</form:option>
                                    <form:option value="CH">CH</form:option>
                                    <form:option value="JP">JP</form:option>
                                    <form:option value="HU">HU</form:option>

                                    <form:option value="??">??</form:option>
                                </form:select>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="welcome.name"/></td>
                            <td><form:input path="name"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="welcome.city"/></td>
                            <td><form:input path="city" onblur="wyswietlMarker($('#city').val());"/></td>
                        </tr>

                        <tr>
                            <td><input type="submit" value="Create"/></td>
                            <!-- <td></td> -->
                        </tr>

                        <tr>
                            <td><form:input path="clong" id="clongv" type="hidden"/></td>
                            <td><form:input path="clat" id="clatv" type="hidden"/></td>
                        </tr>


                        </tbody>
                    </table>
                    </form:form>
    </div>
    <br><br><br><br><br></br></br></br></br></br>
    </td>
    <td>
        <style>
            #mapCanvas {
                width: 500px;
                height: 400px;
                float: left;
            }

            #infoPanel {
                float: left;
                margin-left: 10px;
            }

            #infoPanel div {
                margin-bottom: 5px;
            }
        </style>

        <div id="mapCanvas"></div>
        <div id="infoPanel">


        </div>
    </td>
    </tr></table>
</body>
</html>