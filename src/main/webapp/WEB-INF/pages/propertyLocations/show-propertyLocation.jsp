<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><spring:message code="location.show_all_title"/></title>

    <style type="text/css">
        #map {
        <!-- width : 800 px;
        height : 600 px;
        -->
        }

        #map_wrap {
            position: relative;
            width: 100%;
            padding-top: 0px;
            border: 1px solid #aaa;
        }
    </style>

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

    <script type="text/javascript">
        var map;
        var markers = [];

        function initialize() {
            var options = {
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.TERRAIN],
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                }
            };

            var pierwszyWysokosc = '<c:out value="${coordinatesList[0].wysokosc}" />';
            var pierwszySzerokosc = '<c:out value="${coordinatesList[0].szerokosc}" />';
            map = new google.maps.Map(document.getElementById('map'));
            map.setCenter(new google.maps.LatLng(pierwszyWysokosc, pierwszySzerokosc));
            map.setZoom(2);
            map.setMapTypeId(google.maps.MapTypeId.ROADMAP);

            <c:forEach items="${communicationDialects}" var="p">
            addMarker(new google.maps.LatLng(${p.clat}, ${p.clong}), "");
            </c:forEach>

            <c:forEach items="${buildings}" var="p">
            addMarker(new google.maps.LatLng(${p.clat}, ${p.clong}), "");
            </c:forEach>

        }
        function addMarker(latlng, myTitle) {
            markers.push(new google.maps.Marker({
                position: latlng,
                map: map,
                title: myTitle,
                icon: "http://maps.google.com/mapfiles/marker" + ".png"
            }));
        }
    </script>

</head>
    <body>
        <h1><spring:message code="location.show_all_title"/></h1>
        <div id="map_wrap" align="center">
            <div id="map"></div>
        </div>
        <script type="text/javascript">
            initialize();
        </script>
    </body>
</html>