<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="location.show_all_title"/></title>
    <link href="resources/css/main.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            var deleteLink = $("a:contains('Delete')");

            $(deleteLink).click(function (event) {

                $.ajax({
                    url: $(event.target).attr("href"),
                    type: "DELETE",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },

                    success: function (propertyLocation) {
                        var respContent = "";
                        var rowToDelete = $(event.target).closest("tr");

                        rowToDelete.remove();

                        respContent += "<span class='success'> PropertyLocation was deleted: [";
                        respContent += propertyLocation.name + " : ";
                        respContent += propertyLocation.country + " : ";
                        respContent += propertyLocation.city + "]</span>";

                        $("#transmissionFromResponse").html(respContent);
                    }
                });

                event.preventDefault();
            });

        });
    </script>

</head>
<body>

<div id="all" class="all">
    <%@include file="../menu.jsp" %>

    <div id="container">
        <h1><spring:message code="location.show_all_title"/></h1>

        <div>
            <div id="transmissionFromResponse"></div>
        </div>
        <table <%--border="1px" cellpadding="2" cellspacing="2" width="800"--%>>
            <thead>
            <tr>
                <th><spring:message code="welcome.name"/></th>
                <th><spring:message code="welcome.city"/></th>
                <th><spring:message code="welcome.country"/></th>
                <!-- <th><spring:message code="welcome.clat"/></th> -->
                <!-- <th><spring:message code="welcome.clong"/></th> -->
                <th><spring:message code="welcome.options"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="transmission" items="${propertyLocations}">
                <tr>
                    <td>${transmission.name}</td>
                    <td>${transmission.city}</td>
                    <td>${transmission.country}</td>
                    <!-- <td>${transmission.clat}</td> -->
                    <!-- <td>${transmission.clong}</td> -->

                    <td>
                        <a name="edit${transmission.id}" href="${pageContext.request.contextPath}/propertyLocations/edit/${transmission.id}.html">Edit </a><br/>
                        <a name="xml${transmission.id}" href="${pageContext.request.contextPath}/propertyLocations/xml/${transmission.id}.xml">To Xml </a><br/>
                        <a name="del${transmission.id}" href="${pageContext.request.contextPath}/propertyLocations/delete/${transmission.id}.json">Delete </a><br/>

                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <%--<a href="${pageContext.request.contextPath}/propertyLocations/generate-xml.html">Generate xml with
            propertyLocations</a><br/>--%>
    </div>
</div>
</body>
</html>