<?xml version="1.0" encoding="UTF-8" ?>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>--%>
        <meta http-equiv="refresh" content="0; URL=${pageContext.request.contextPath}/devices.html?page=devices">
        <title><spring:message code="index.title"/></title>

        <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet" type="text/css"/>
        <%--<link href="/masterOfHouse/resources/mytheme/css/main.css" rel="stylesheet" type="text/css"/>--%>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"
                script type="text/javascript"></script>

    </head>


    <body>
    <%--  <div style="background-image: url('<c:url value="/resources/mytheme/img/pixel.png" />');"> <br><br><br></div> --%>
        <div id="all" class="all">
            <%--Menu and banner included from external file--%>
            <%@include file="menu.jsp" %>
            <h1><spring:message code="index.title"/></h1>
            <div id="container">
                <p><spring:message code="index.description"/></p>
            </div>
        </div>
    </body>
</html>