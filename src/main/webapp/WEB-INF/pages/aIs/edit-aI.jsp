<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
    <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet">

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzvmWZOjMUJd4F5RF2x4tQhvgoHZ3V99k&sensor=false">
    </script>

    <script type="text/javascript">
        var geocoder = new google.maps.Geocoder();
        var vlong = 0;
        var vlng = 0;
        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            });
        }
        function wyswietlMarker(address) {
            geocoder.geocode({'address': address},
                    function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            firstLoc = results[0].geometry.location;
                            map = new google.maps.Map(document.getElementById("mapCanvas"),
                                    {
                                        center: firstLoc,
                                        zoom: 15,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    });
                            var iconBase = 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/64/';
                            var marker = new google.maps.Marker({
                                position: firstLoc,
                                map: map,
                                draggable: true,
                                icon: iconBase + 'Map-Marker-Flag-2-Right-Azure-icon.png'

                            });

                            // Update current position info.
                            updateMarkerPosition(firstLoc);
                            geocodePosition(firstLoc);
                            marker.setAnimation(google.maps.Animation.BOUNCE);


                            google.maps.event.addListener(marker, 'drag', function () {
                                updateMarkerPosition(marker.getPosition());
                            });

                            google.maps.event.addListener(marker, 'dragend', function () {
                                geocodePosition(marker.getPosition());
                            });
                        }
                        else {
                            alert("Nie moge znaleźć adresu: " + address);
                        }
                    }
            );
        }

        function updateMarkerStatus(str) {
            document.getElementById('markerStatus').innerHTML = str;
        }

        function updateMarkerPosition(latLng) {

            document.getElementById("clatv").value = latLng.lat();
            document.getElementById("clongv").value = latLng.lng();

            vlong = latLng.lng();
            vlat = latLng.lat();
        }

        function initialize() {

            var latLng = new google.maps.LatLng(51.108260661933365, 17.060721343994146);
            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 1,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var iconBase = 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/64/';
            var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                draggable: true,
                icon: iconBase + 'Map-Marker-Flag-2-Right-Azure-icon.png'

            });

            // Update current position info.
            updateMarkerPosition(latLng);
            geocodePosition(latLng);
            marker.setAnimation(google.maps.Animation.BOUNCE);


            google.maps.event.addListener(marker, 'drag', function () {
                updateMarkerPosition(marker.getPosition());
            });

            google.maps.event.addListener(marker, 'dragend', function () {
                geocodePosition(marker.getPosition());
            });
        }

        // Onload handler to fire off the app.
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="aI.n_titile"/></title>
    <link href="../resources/css/main.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#newAIFrom').submit(function (event) {

                var tag = $('#tag').val();
                var name = $('#name').val();
                var clong = vlong;
                var clat = vlat;
                var open_hours = $('#open_hours').val();
                var address = $('#address').val();
                var floor = $('#floor').val();
                var room = $('#room').val();
                var description = $('#description').val();
                var phone = $('#phone').val();
                var mail = $('#mail').val();
                var www = $('#www').val();
                var json = {
                    "tag": tag,
                    "name": name,
                    "clat": clat,
                    "clong": clong,
                    "open_hours": open_hours,
                    "address": address,
                    "floor": floor,
                    "room": room,
                    "description": description,
                    "phone": phone,
                    "mail": mail,
                    "www": www,
                };

                $.ajax({
                    url: $("#newAIFrom").attr("action"),
                    data: JSON.stringify(json),
                    type: "POST",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (aI) {
                        var respContent = "";

                        respContent += "<span class='success'>AIs was created: [";

                        respContent += aI.tag + " : ";
                        respContent += aI.name + " : ";
                        respContent += aI.clat + " : ";
                        respContent += aI.clong + " : ";
                        respContent += aI.open_hours + " : ";
                        respContent += aI.address + " : ";
                        respContent += aI.floor + " : ";
                        respContent += aI.room + " : ";
                        respContent += aI.description + " : ";
                        respContent += aI.phone + " : ";
                        respContent += aI.mail + " : ";
                        respContent += aI.www + " : ";

                        $("#sPhoneFromResponse").html(respContent);
                    }
                });

                event.preventDefault();
            });

        });
    </script>


</head>


<body>

<div id="all" class="all">
    <%@include file="../menu.jsp" %>

    <div id="container">
        <h1><spring:message code="aI.n_titile"/></h1>

        <div>
            <p><spring:message code="aI.ns_titile"/></p>

            <div id="sPhoneFromResponse"></div>
        </div>
        <table>
            <tr>
                <td>
                    <form:form id="newAIFrom"
                               action="${pageContext.request.contextPath}/aIs/edit/${aI.id}.json"
                               commandName="aI">
                    <table>
                        <tbody>


                        <tr>
                            <td><spring:message code="welcome.tag"/></td>
                            <td><form:input path="tag"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="welcome.name"/></td>
                            <td><form:input path="name"/></td>
                        </tr>


                        <tr>
                            <td><spring:message code="welcome.open_hours"/></td>
                            <td><form:input path="open_hours"/></td>
                        </tr>

                        <tr>
                            <td><spring:message code="welcome.address"/>:</td>
                            <td><form:input path="address" onblur="wyswietlMarker($('#address').val());"/></td>
                        </tr>


                        <tr>
                            <td><spring:message code="welcome.floor"/>:</td>
                            <td><form:input path="floor"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="welcome.room"/>:</td>
                            <td><form:input path="room"/></td>
                        </tr>

                        <tr>
                            <td><spring:message code="welcome.description"/>:</td>
                            <td><form:textarea rows="5" cols="19" path="description"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="welcome.phone"/></td>
                            <td><form:input path="phone"/></td>
                        </tr>

                        <tr>
                            <td><spring:message code="welcome.mail"/></td>
                            <td><form:input path="mail"/></td>
                        </tr>
                        <tr>
                            <td><spring:message code="welcome.www"/>:</td>
                            <td><form:input path="www"/></td>
                        </tr>

                        <tr>
                            <td><form:input path="clong" id="clongv" type="hidden"/></td>
                            <td><form:input path="clat" id="clatv" type="hidden"/></td>
                        </tr>

                        <tr>
                            <td><input type="submit" value="    OK   "/></td>
                            <td></td>
                        </tr>

                        </tbody>
                    </table>
                    </form:form>
    </div>


    </td>
    <td>

        <style>
            #mapCanvas {
                width: 500px;
                height: 400px;
                float: left;
            }

            #infoPanel {
                float: left;
                margin-left: 10px;
            }

            #infoPanel div {
                margin-bottom: 5px;
            }
        </style>

        <div id="mapCanvas"></div>
        <div id="infoPanel">


        </div>
    </td>
    </tr></table>
</div>
</body>
</html>