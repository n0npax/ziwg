<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="<c:url value="/resources/mytheme/css/main.css" />" rel="stylesheet">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="aI.a_title"/></title>
    <link href="resources/css/main.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            var deleteLink = $("a:contains('Delete')");

            $(deleteLink).click(function (event) {

                $.ajax({
                    url: $(event.target).attr("href"),
                    type: "DELETE",

                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },

                    success: function (buildings) {
                        var respContent = "";
                        var rowToDelete = $(event.target).closest("tr");

                        rowToDelete.remove();

                        respContent += "<span class='success'>building was deleted: [";

                        respContent += building.name + "]</span>";

                        $("#sAIFromResponse").html(respContent);
                    }
                });

                event.preventDefault();
            });

        });
    </script>

</head>
<body>

<div id="all" class="all">
    <%@include file="../menu.jsp" %>

    <div id="container">
        <h1><spring:message code="aI.a_title"/></h1>

        <div>
            <p><spring:message code="aI.as_title"/></p>

            <div id="sAIFromResponse"></div>
        </div>

        <c:forEach var="u" items="${places}">
            <a href="${pageContext.request.contextPath}/aIs/buildingsFromPropertyLocation/${u.id}">${u.name}</a>
        </c:forEach>
        <br>
        <c:forEach var="u" items="${buildings}">
            <a href="${pageContext.request.contextPath}/aIs/devicesFromBuilding/${u.id}">${u.name}</a>
        </c:forEach>
        <br>
        <c:forEach var="u" items="${devices}">
           ${u.name}
        </c:forEach>

        <table border="1px" cellpadding="2" cellspacing="2" width="800">
            <thead>
            <tr>
                <th><spring:message code="welcome.name"/></th>

            </tr>
            </thead>
            <tbody>
            <c:forEach var="sAI" items="${aIs}">
                <tr>
                    <td>${sAI.name}</td>
                    <td>
                        <a name="xml${sAI.id}" href="${pageContext.request.contextPath}/aIs/xml/${sAI.id}.xml">To Xml</a><br/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

    </div>
</div>
</body>
</html>