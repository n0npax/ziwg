package com.pwr.masterOfHouse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.pwr.masterOfHouse.ai.HistoryComplementer;
import com.pwr.masterOfHouse.model.DeviceValues;
import com.pwr.masterOfHouse.repository.DeviceRepository;
import com.pwr.masterOfHouse.repository.DeviceValuesRepository;

public class HistoryComplementerTests extends IntegrationTest {

	@Autowired
	HistoryComplementer history;
	@Autowired
	DeviceRepository devRepo;
	@Autowired
	DeviceValuesRepository devValuesRepo;

	static String text;

	@BeforeClass
	public static void setUp() {
		text = "aaa";
	}

	@Test
	public void historyShouldNotBeNull() {
		assertNotNull(history);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void methodsShouldPersistData() throws ParseException {
		try {
			history.persistReadFromDevice(text, devRepo.findOne(13));
			DeviceValues val = devValuesRepo.findByDeviceOrderByDateDesc(
					devRepo.findOne(13)).get(0);
			Date date = new Date();
			assertEquals(val.getDate().getYear(), date.getYear());
			assertEquals(val.getDate().getMonth(), date.getMonth());
			assertEquals(val.getDate().getDay(), date.getDay());
			assertEquals(val.getDate().getHours(), date.getHours());
			assertEquals(val.getDate().getMinutes(), date.getMinutes());
			if (val.getDate().getSeconds() != date.getSeconds()
					&& val.getDate().getSeconds() != date.getSeconds() - 1)
				fail();
			assertEquals(val.getDevice().getId(), new Integer(13));
			assertEquals(val.getValues(), text);
		} finally {
			DeviceValues val = devValuesRepo.findByDeviceOrderByDateDesc(
					devRepo.findOne(13)).get(0);
			if (val.getValues().equals(text))
				devValuesRepo.delete(val);
		}
	}
}
