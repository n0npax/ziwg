package com.pwr.masterOfHouse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.pwr.masterOfHouse.model.DeviceValues;
import com.pwr.masterOfHouse.repository.DeviceRepository;
import com.pwr.masterOfHouse.repository.DeviceValuesRepository;

public class DeviceValuesRepositoryTests extends IntegrationTest {

	@Autowired
	DeviceValuesRepository repo;
	@Autowired
	DeviceRepository devRepo;

	@Test
	public void repoShouldNotBeNull() {
		assertNotNull(repo);
	}

	@Test
	public void findByDeviceOrderByDateDescShouldWork() {
		List<DeviceValues> list = repo.findByDeviceOrderByDateDesc(devRepo
				.findOne(13));
		assertEquals(list.size(), 2);
		DeviceValues dev0 = list.get(0);
		DeviceValues dev1 = list.get(1);
		assertEquals(dev0.getValues(),
				"<device><name>abc</name><value>5</value></device>");
		assertEquals(dev0.getDate().toString(), "2015-04-06 15:01:00.0");
		assertEquals(dev0.getDevice().getId(), new Integer(13));
		assertEquals(dev1.getValues(),
				"<device><name>abc</name><value>4</value></device>");
		assertEquals(dev1.getDate().toString(), "2015-04-06 15:00:00.0");
		assertEquals(dev1.getDevice().getId(), new Integer(13));
	}

	@Test
	public void setByUserTest() {
		DeviceValues val1 = repo.currentValueForDevice(devRepo.findOne(13));
		DeviceValues val2 = repo.userSettingForDevice(devRepo.findOne(13));
		assertEquals(val1.getDate().toString(), "2015-04-06 15:01:00.0");
		assertEquals(val1.getValues(),
				"<device><name>abc</name><value>5</value></device>");
		assertEquals(val1.isSetByUser(), false);
		assertEquals(val1.getDevice().getId(), new Integer(13));
		assertEquals(val2.getDate().toString(), "2015-04-06 15:00:00.0");
		assertEquals(val2.getValues(),
				"<device><name>abc</name><value>4</value></device>");
		assertEquals(val2.isSetByUser(), true);
		assertEquals(val2.getDevice().getId(), new Integer(13));
	}
}
