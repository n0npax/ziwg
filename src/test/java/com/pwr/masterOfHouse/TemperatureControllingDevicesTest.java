package com.pwr.masterOfHouse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.pwr.masterOfHouse.model.TemperatureControllingDevice;
import com.pwr.masterOfHouse.repository.BuildingRepository;
import com.pwr.masterOfHouse.repository.DeviceRepository;

public class TemperatureControllingDevicesTest extends IntegrationTest {

	@Autowired
	DeviceRepository repo;
	@Autowired
	BuildingRepository buildingRepo;

	@Test
	public void deviceRepositoryShouldNotBeNull() {
		assertNotNull(repo);
	}

	@Test
	public void findTemperatureControllingDevicesShouldReturnCorrectResults() {
		List<TemperatureControllingDevice> devs = repo
				.getDevicesForTemperatureControlling(buildingRepo.findOne(13));
		assertEquals(devs.size(), 1);
		assertEquals(devs.get(0).getTag(), "dev3");
	}
}
