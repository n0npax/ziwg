package com.pwr.masterOfHouse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.pwr.masterOfHouse.model.Device;
import com.pwr.masterOfHouse.repository.DeviceRepository;

public class BasicTransactionsTests extends IntegrationTest {

	@Autowired
	DeviceRepository repo;

	@Test
	public void repoShouldNotBeNull() {
		assertNotNull(repo);
	}

	@Test
	public void repoShouldBeAbleToFindAll() {
		repo.findAll();
	}

	@Test
	public void repoShouldFindDeviceWithId13AndValidateData() {
		Device device = repo.findOne(13);
		assertNotNull(device);
		assertEquals(device.getTag(), "dev3");
		assertEquals(device.getName(), "lala");
		assertEquals(device.getEditable(), true);
		assertEquals(device.getDescription(), "lal");
		assertEquals(device.getXmlDevDesc(), "lala");
		assertEquals(device.getWww(), "lala");
		assertEquals(device.isTemperatureControlling(), true);
		assertEquals(device.getCommunicationDialect().getId(), new Integer(13));
		assertEquals(device.getBuilding().getId(), new Integer(13));
		assertEquals(device.getIsConfirmed(), true);
	}

}
