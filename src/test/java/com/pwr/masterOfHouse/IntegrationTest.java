package com.pwr.masterOfHouse;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.pwr.masterOfHouse.init.AIConfig;
import com.pwr.masterOfHouse.init.WebAppConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { AIConfig.class, WebAppConfig.class })
public abstract class IntegrationTest {
}
