#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'n0npax'

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import mapper, sessionmaker

from config_parser import ConfigParser
from models.db_objects import *
from logger import log_error, log_warn, log_critical, log_info


class PersistenceManager(object):

    engine = None
    session = None
    metadata = None

    @staticmethod
    @log_info
    def load_session(Debug = False):
        if PersistenceManager.session:
            return PersistenceManager.session
        db_conf = ConfigParser.get_db_config()
        PersistenceManager.engine = create_engine('mysql+mysqldb://{username}:{password}@{url}/{database}'.format(**db_conf), echo=Debug)
        PersistenceManager.metadata = MetaData(PersistenceManager.engine)
        #map some of tables from masterOfHouse
        moz_propertyLocations = Table('propertyLocations', PersistenceManager.metadata, autoload=True)
        mapper(PropertyLocations, moz_propertyLocations)
        moz_devices = Table('devices', PersistenceManager.metadata, autoload=True)
        mapper(Devices, moz_devices)
        moz_communicationDialects = Table('communicationDialects', PersistenceManager.metadata, autoload=True)
        mapper(CommunicationDialects, moz_communicationDialects)
        moz_buildings = Table('buildings', PersistenceManager.metadata, autoload=True)
        mapper(Buildings, moz_buildings)
        moz_devicevalues = Table('deviceValues', PersistenceManager.metadata, autoload=True)
        mapper(Devicevalues, moz_devicevalues)

        Session = sessionmaker(bind=PersistenceManager.engine)
        PersistenceManager.session = Session()
        return PersistenceManager.session

    @staticmethod
    @log_warn
    def add_Obj_To_db(obj_type, args_dict):
        my_obj = obj_type()
        fields = [x for x in dir(my_obj) if not x.startswith('_')]
        for key_arg, _ in args_dict.items():
            if not (key_arg) in fields:
                raise Exception('Wrong key attribute!\n %s not in %s' % (key_arg, fields))
        for k, v in args_dict.items():
            setattr(my_obj, k, v)
        PersistenceManager.session.add(my_obj)
        PersistenceManager.session.commit()
        return my_obj

    @staticmethod
    @log_warn
    def get_all_objects(obj_type):
        return PersistenceManager.session.query(obj_type).all()

    @staticmethod
    @log_warn
    def delete_object(obj):
        PersistenceManager.session.delete(obj)
        PersistenceManager.session.commit()

    @staticmethod
    @log_warn
    def get_obj_by(obj_type, query_key, value, compare_type='=='):
        if compare_type == '==':
            return PersistenceManager.session.query(obj_type).filter(getattr(obj_type, query_key) == value).all()
        elif compare_type == '>':
            return PersistenceManager.session.query(obj_type).filter(getattr(obj_type, query_key) > value).all()
        elif compare_type == '<':
            return PersistenceManager.session.query(obj_type).filter(getattr(obj_type, query_key) < value).all()
        raise Exception("Not supported comparision type! >>%s<<" % compare_type)
