# coding=utf-8
import time
import socket
import xml.etree.cElementTree as ET
from xml.dom import minidom


# Read launch settings from XML file
def read_settings():
    xmldoc = minidom.parse('backdrop/settings.xml')
    desc = xmldoc.getElementsByTagName('description')[0]
    position = xmldoc.getElementsByTagName('value')[0]
    min_position = xmldoc.getElementsByTagName('minposition')[0]
    max_position = xmldoc.getElementsByTagName('maxposition')[0]

    return desc.firstChild.data, position.firstChild.data, \
           min_position.firstChild.data, \
           max_position.firstChild.data


# Make XML file which will be send to collecting server.
def make_xml(desc, position):
    root = ET.Element("backdrop")

    ET.SubElement(root, "title").text = "Backdrop controlled by RPi"
    ET.SubElement(root, "description").text = str(desc)
    ET.SubElement(root, "value").text = str(position)

    tree = ET.ElementTree(root)
    tree.write("backdrop/send.xml")


# Send temperature to the server
def send_position():
    tcp_ip = '127.0.0.1'
    tcp_port = 10001
    buffer_size = 1024
    s = socket.socket()
    s.connect((tcp_ip, tcp_port))
    f = open('backdrop/send.xml', 'rb')
    print "B: sending XML..."
    l = f.read(buffer_size)
    while l:
        s.send(l)
        l = f.read(buffer_size)
    f.close()
    s.close()
    print "B: file sent"


def rec_settings():
    tcp_ip = '127.0.0.1'
    tcp_port = 10003
    buffer_size = 1024
    print "B: wait for settings"
    s = socket.socket()
    s.bind((tcp_ip, tcp_port))
    s.listen(10)
    sc, address = s.accept()

    f = open("backdrop/settings.xml", 'wb')
    l = sc.recv(buffer_size)
    while l:
        f.write(l)
        l = sc.recv(buffer_size)
    f.close()
    print "B: got settings"
    sc.close()
    s.close()


# Start sending server
def start_backdrop():
    desc, position, min_position, max_position = read_settings()
    make_xml(desc, position)
    send_position()
    rec_settings()
    time.sleep(1.0)
    return
