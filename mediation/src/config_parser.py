import configparser
import os

class ConfigParser (object):

    config_path = 'config.ini' if os.path.exists('config.ini') else '../config.ini'

    @staticmethod
    def get_db_config():
        config = configparser.ConfigParser()
        config.read(ConfigParser.config_path)
        return dict(config['db'].items())

    @staticmethod
    def get_logs_config():
        config = configparser.ConfigParser()
        config.read(ConfigParser.config_path)
        return dict(config['logs'].items())

    @staticmethod
    def get_info_config():
        config = configparser.ConfigParser()
        config.read(ConfigParser.config_path)
        return dict(config['info'].items())
