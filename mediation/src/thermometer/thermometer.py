# coding=utf-8
import time
import random
import socket
import xml.etree.cElementTree as ET
from xml.dom import minidom


# Read launch settings from XML file
def read_settings():
    xmldoc = minidom.parse('thermometer/settings.xml')
    desc = xmldoc.getElementsByTagName('description')[0]
    minTemperature = xmldoc.getElementsByTagName('mintemperature')[0]
    maxTemperature = xmldoc.getElementsByTagName('maxtemperature')[0]

    return desc.firstChild.data, minTemperature.firstChild.data, \
           maxTemperature.firstChild.data


# Make XML file which will be send to collecting server.
def make_xml(desc, temperature, minTemperature, maxTemperature):
    root = ET.Element("thermometer")

    ET.SubElement(root, "title").text = "Ethernet thermometer TME designed by Papouch s.r.o. - www.papouch.com"
    ET.SubElement(root, "description").text = str(desc)
    ET.SubElement(root, "value").text = str(temperature)
    ET.SubElement(root, "mintemperature").text = str(minTemperature)
    ET.SubElement(root, "maxtemperature").text = str(maxTemperature)

    tree = ET.ElementTree(root)
    tree.write("thermometer/tme.xml")


# Make random temperature.
def make_temperature():
    return random.randint(180, 250)


# Send temperature to the server
def send_temperature():
    tcp_ip = '127.0.0.1'
    tcp_port = 10001
    buffer_size = 1024
    s = socket.socket()
    s.connect((tcp_ip, tcp_port))
    f = open('thermometer/tme.xml', 'rb')
    print "T: sending XML..."
    l = f.read(buffer_size)
    while l:
        s.send(l)
        l = f.read(buffer_size)
    f.close()
    s.close()
    print "T: file sent"


def rec_settings():
    tcp_ip = '127.0.0.1'
    tcp_port = 10002
    buffer_size = 1024
    print "T: wait for settings"
    s = socket.socket()
    s.bind((tcp_ip, tcp_port))
    s.listen(5)
    sc, address = s.accept()

    f = open("thermometer/settings.xml", 'wb')
    l = sc.recv(buffer_size)
    while l:
        f.write(l)
        l = sc.recv(buffer_size)
    f.close()
    print "T: got settings"
    sc.close()
    s.close()


# Start sending server
def send_server():
    desc, minTemperature, maxTemperature = read_settings()
    temperature = make_temperature()
    make_xml(desc, temperature, minTemperature, maxTemperature)
    send_temperature()
    rec_settings()
    time.sleep(1.0)
