__author__ = 'n0npax'
import logging
import os
from config_parser import ConfigParser

__config_filename = ConfigParser.get_logs_config()['path']
if os.path.isdir(__config_filename): __config_filename += '/logs.log'
__logger = logging.getLogger(__config_filename)
logging.basicConfig(filename=__config_filename, level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def log_info(func):
    def func_wrapper(*args, **kwargs):
        try:
            __logger.info(func.__name__)
            return func(*args, **kwargs)
        except Exception as e:
            __logger.error(str(e)+'\targs: '+str(args)+'\tkwargs:'+str(kwargs))
            raise e
    return func_wrapper

def log_warn(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            __logger.warn(str(e)+'\targs: '+str(args)+'\tkwargs:'+str(kwargs))
            raise e
    return func_wrapper

def log_error(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            __logger.error(str(e)+'\targs: '+str(args)+'\tkwargs:'+str(kwargs))
            raise e
    return func_wrapper

def log_critical(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            __logger.critical(str(e)+'\targs: '+str(args)+'\tkwargs:'+str(kwargs))
            raise e
    return func_wrapper

def vanish_exception(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except: pass
    return func_wrapper
