import smbus
import time

BUS_NUMBER = 1
DEVICE_ADDR = 0x48

bus = smbus.SMBus(BUS_NUMBER)
bus.write_byte(DEVICE_ADDR,0x00)

while True:
	value = bus.read_byte(DEVICE_ADDR)
	print value 
	time.sleep(0.5)
