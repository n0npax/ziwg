# coding=utf-8
import random
import socket
import xml.etree.cElementTree as ET
import smbus
import time
from xml.dom import minidom

BUS_NUMBER = 1
DEVICE_ADDR = 0x48
server_addr = "192.168.0.100"

# Make XML file which will be send to collecting server.
def make_xml(desc, value):
    root = ET.Element("potentiometer")

    ET.SubElement(root, "title").text = str(desc)
    ET.SubElement(root, "description").text = str(desc)
    ET.SubElement(root, "value").text = str(value)

    tree = ET.ElementTree(root)
    tree.write("potentiometer.xml")


# Send value to the server
def send_value():
    tcp_ip = server_addr
    tcp_port = 10001
    buffer_size = 1024
    s = socket.socket()
    s.connect((tcp_ip, tcp_port))
    f = open('potentiometer.xml', 'rb')
    print "T: sending XML..."
    l = f.read(buffer_size)
    while l:
        s.send(l)
        l = f.read(buffer_size)
    f.close()
    s.close()
    print "T: file sent"


# Start sending server
def send_server():
    bus = smbus.SMBus(BUS_NUMBER)
    bus.write_byte(DEVICE_ADDR, 0x00)
    desc = "Potentiometer"
    while True:
        value = bus.read_byte(DEVICE_ADDR)
        make_xml(desc, value)
        send_value()
        time.sleep(1.0)
