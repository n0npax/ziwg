from unittest import TestCase
from logger import log_critical, log_error, log_info, log_warn, vanish_exception
from config_parser import ConfigParser
__author__ = 'n0npax'


class TestLog_info(TestCase):

    def tearDown(self):
        with open('logs.log', 'w') as f:
            f.write('')

    def test_vanish_exception(self):
        @vanish_exception
        def fun(x, y):
            return x/y
        try:
            fun(0, 0)
        except:
            self.fail()

    def test_log_info(self):

        @log_info
        def fun(x, y):
            return x/y
        try:
            fun(0, 0)
        except:
            pass
        finally:
            with open('logs.log', 'r') as f:
                data = f.read()
                self.assertEquals(True, 'INFO' in data)
                self.assertEquals(True, 'ERROR' in data)

    def test_log_warn(self):
        @log_warn
        def fun(x, y):
            return x/y
        try:
            fun(0, 0)
        except:
            pass
        finally:
            with open('logs.log', 'r') as f:
                data = f.read()
                self.assertEquals(True, 'WARNING' in data)

    def test_log_error(self):
        @log_error
        def fun(x, y):
            return x/y
        try:
            fun(0, 0)
        except:
            pass
        finally:
            with open('logs.log','r') as f:
                data = f.read()
                self.assertEquals(True, 'ERROR' in data)

    def test_log_critical(self):
        @log_critical
        def fun(x, y):
            return x/y
        try:
            fun(0, 0)
        except:
            pass
        finally:
            with open('logs.log', 'r') as f:
                data = f.read()
                self.assertEquals(True, 'CRITICAL' in data)
