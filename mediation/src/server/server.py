# coding=utf-8
import os
import sys
import socket
from threading import Lock
import xml.etree.cElementTree as ET
from xml.dom import minidom
import datetime
from persistence_manager import PersistenceManager
from models.db_objects import *
from sqlalchemy import update
import random


def rec_xml():
    tcp_ip = '127.0.0.1'
    tcp_port = 10001
    buffer_size = 1024
    s = socket.socket()
    print "Server: waiting for connection"
    s.bind((tcp_ip, tcp_port))
    s.listen(10)
    print "Server: connected"
    sc, address = s.accept()

    f = open('server/file.xml', 'wb')
    l = sc.recv(buffer_size)
    while l:
        f.write(l)
        l = sc.recv(buffer_size)
    f.close()
    print "Server: got settings"
    sc.close()
    s.close()

def read_xml():
    xmldoc = minidom.parse('server/file.xml')
    title = xmldoc.getElementsByTagName('title')[0]
    if str(title.firstChild.data) == "Ethernet thermometer TME designed by Papouch s.r.o. - www.papouch.com":
        choice = 1
    elif str(title.firstChild.data) == "Backdrop controlled by RPi":
        choice = 2
    elif str(title.firstChild.data) == "Potentiometer":
        choice = 3
    else:
        choice = 0
    return choice


def read_thermometer_xml():
    xmldoc = minidom.parse('server/file.xml')
    temperature = xmldoc.getElementsByTagName('value')[0]

    return temperature.firstChild.data


def send_to_db_the(temperature):
    session = PersistenceManager.load_session()

    device = PersistenceManager.get_obj_by(Devices, 'name', 'Ethernet thermometer TME', '==')[0]
    file = device.devvalues
    xmldoc = minidom.parseString(file)
    desc = xmldoc.getElementsByTagName('description')[0]
    minTemperature = xmldoc.getElementsByTagName('mintemperature')[0]
    maxTemperature = xmldoc.getElementsByTagName('maxtemperature')[0]

    write_thermoterer_xml(minTemperature.firstChild.data, maxTemperature.firstChild.data, temperature, desc.firstChild.data)
    f = open('server/tme.xml', 'rb')
    new = f.readline()
    device.devvalues = new

    tcp_ip = '127.0.0.1'
    tcp_port = 10002
    buffer_size = 1024
    s = socket.socket()
    s.connect((tcp_ip, tcp_port))
    f = open('server/tme.xml', 'rb')
    print "S: sending thermometer settings..."
    l = f.read(buffer_size)
    while l:
        s.send(l)
        l = f.read(buffer_size)
    f.close()
    s.close()
    print "S: file sent"

    session.commit()

    print minTemperature.firstChild.data + " < " + temperature + " < " + maxTemperature.firstChild.data + "\n"


def write_thermoterer_xml(minTemperature, maxTemperature, temperature, desc):
    root = ET.Element("thermometer")

    ET.SubElement(root, "title").text = "Ethernet thermometer TME designed by Papouch s.r.o. - www.papouch.com"
    ET.SubElement(root, "description").text = str(desc)
    ET.SubElement(root, "value").text = str(temperature)
    ET.SubElement(root, "mintemperature").text = str(minTemperature)
    ET.SubElement(root, "maxtemperature").text = str(maxTemperature)

    tree = ET.ElementTree(root)
    tree.write("server/tme.xml")


def read_position_xml():
    xmldoc = minidom.parse('server/file.xml')
    desc = xmldoc.getElementsByTagName('description')[0]
    position = xmldoc.getElementsByTagName('value')[0]

    return position.firstChild.data

def send_to_db_pos(position):
    session = PersistenceManager.load_session()

    device = PersistenceManager.get_obj_by(Devices, 'name', 'Backdrop controlled by RPi', '==')[0]
    file = device.devvalues
    xmldoc = minidom.parseString(file)
    desc = xmldoc.getElementsByTagName('description')[0]
    min_position = xmldoc.getElementsByTagName('minposition')[0]
    max_position = xmldoc.getElementsByTagName('maxposition')[0]

    write_position_xml(desc.firstChild.data, position, min_position.firstChild.data, max_position.firstChild.data)
    f = open('server/pos.xml', 'rb')
    new = f.readline()
    device.devvalues = new

    tcp_ip = '127.0.0.1'
    tcp_port = 10003
    buffer_size = 1024
    s = socket.socket()
    s.connect((tcp_ip, tcp_port))
    f = open('server/pos.xml', 'rb')
    print "S: sending position settings..."
    l = f.read(buffer_size)
    while l:
        s.send(l)
        l = f.read(buffer_size)
    f.close()
    s.close()
    print "S: file sent"

    session.commit()

    print "position: " + position

def write_position_xml(desc, position, min_position, max_position):
    root = ET.Element("backdrop")

    ET.SubElement(root, "title").text = "Backdrop controlled by RPi"
    ET.SubElement(root, "description").text = str(desc)
    ET.SubElement(root, "value").text = str(position)
    ET.SubElement(root, "minposition").text = str(min_position)
    ET.SubElement(root, "maxposition").text = str(max_position)

    tree = ET.ElementTree(root)
    tree.write("server/pos.xml")


def send_to_db_pot():
    session = PersistenceManager.load_session()

    device = PersistenceManager.get_obj_by(Devices, 'name', 'Potentiometer', '==')[0]
    f = open('server/file.xml', 'rb')
    device.devvalues = f.readline()

    session.commit()


def start_server():
    lock = Lock()
    while True:
        lock.acquire()
        rec_xml()
        lock.release()
        choice = read_xml()
        if choice == 1:
            temperature = read_thermometer_xml()
            send_to_db_the(temperature)
        elif choice == 2:
            position = read_position_xml()
            send_to_db_pos(position)
        elif choice == 3:
            send_to_db_pot()
            pass
        else:
            pass