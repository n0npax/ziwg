from unittest import TestCase
import os

from report_generator import ReportGenerator


__author__ = 'n0npax'

fn = "test_report.pdf"

class TestReport(TestCase):

    def setUp(self):
        os.remove(fn) if os.path.exists(fn) else None

    def test_pdf_generation(self):
        rg = ReportGenerator()
        rg.create_pdf(fn)
        self.assertEquals(True, os.path.exists(fn))

