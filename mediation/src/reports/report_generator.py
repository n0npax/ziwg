#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'n0npax'

import platform
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from models.db_objects import *
from persistence_manager import  PersistenceManager
from config_parser import ConfigParser
from pdf_generator import PdfGenerator


class ReportGenerator(object):

    pm = PersistenceManager()
    session = PersistenceManager.load_session()
    tables = pm.metadata.tables.keys()

    @staticmethod
    def get_all_objects_from_db():
        for db_table in ReportGenerator.tables:
            obj = globals()[db_table[0].upper() + db_table[1:]]
            yield PersistenceManager.get_all_objects(obj), obj.__name__

    @staticmethod
    def get_fields_from_object(obj):
        for field in dir(obj):
            if not field.startswith('_'):
                yield field

    @staticmethod
    def get_all_objects_in_dict():
        ret =[]
        for items, obj_name in ReportGenerator.get_all_objects_from_db():
            if items:
                row_lst=[{'table_name':obj_name}]
                for item in items:
                    row={}
                    for field in ReportGenerator.get_fields_from_object(item):
                        row[field] = getattr(item,field)
                    row_lst.append(row)
                ret.append(row_lst)
            else:
                ret.append([{'table_name':obj_name}])
        return ret


    @staticmethod
    def create_pdf(report_name='report.pdf'):
        abstract='''
        This report show base information about Master of House System.
        Report contains information about System and Running service'''
        pdf_generator = PdfGenerator(abstract,'System Info', ReportGenerator.get_system_config() ,'Machine Info',ReportGenerator.get_machine_info())
        pdf_generator.page_break()

        for obj_list in ReportGenerator.get_all_objects_in_dict():
            pdf_generator.header(obj_list[0]['table_name'])
            if len( obj_list) ==1:
                pdf_generator.p('No Data')
                continue
            data = [[] for x in range((len(obj_list)))]
            data[0] = obj_list[1].keys()
            i=1
            for obj in obj_list[1:]:
                data_row=[]
                for  obj_value in obj.values():
                    data_row.append(obj_value)
                data[i]=(data_row)
                i+=1
            data = ReportGenerator.parse_table_data(data)
            pdf_generator.p("Table has {0} rows".format(len(data)-1))
            pdf_generator.table(data)

        return pdf_generator.generate_pdf(report_name)
         

    @staticmethod
    def get_system_config():
        config_info = ''
        for key, value in ConfigParser.get_info_config().items():
            config_info += "{:<15}".format(key)+" : {}\n".format(value)
        return  config_info


    @staticmethod
    def get_machine_info():
        system_info  = 'system        : %s\n'% platform.system()
        system_info += 'distribution  : %s\n'%' '.join(platform.dist())
        system_info += 'node          : %s\n'% platform.node()
        system_info += 'release       : %s\n'% platform.release()
        system_info += 'version       : %s\n'% platform.version()
        system_info += 'machine       : %s\n'% platform.machine()
        return system_info

    @staticmethod
    def parse_table_data(data):
        index = data[0].index('id')
        for i in range(len(data)):
            data[i][index],data[i][0] = data[i][0],data[i][index]
        return data

#if __name__ == "__main__":
#    rg = ReportGenerator()
#    print rg.create_pdf()
