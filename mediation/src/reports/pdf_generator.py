#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from reportlab.platypus   import *
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config  import defaultPageSize
from reportlab.lib.units  import inch

from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle

from datetime import date

class PdfGenerator(object):

    PAGE_HEIGHT=defaultPageSize[1]
    styles = getSampleStyleSheet()
    pageinfo = "Master Of House"
    Elements = []
    HeaderStyle = styles["Heading1"]
    HeaderStyle_small = styles['Heading3']
    PreStyle = styles["Code"]
    ParaStyle = styles["Normal"]

    def __init__(self, abstract='',subtitle1='', text1='', subtitle2='', text2=''):
        self.Title=''' Generation time: {0} '''.format(date.today().strftime("%A: %d %b %Y"))
        self.header("REPORT")

        if abstract:
            self.p(abstract)

        if subtitle1:
            self.header_small(subtitle1)
            self.pre(text1)
        if subtitle2:
            self.header_small(subtitle2)
            self.pre(text2)

    def myFirstPage(self,canvas, doc):
        canvas.drawImage("img/home.png", 320, 36)
        canvas.saveState()
        canvas.setStrokeColorRGB(0,1,0.4)
        canvas.setLineWidth(5)
        canvas.line(66,72,66,self.PAGE_HEIGHT-72)
        canvas.line(66,710,550,710)
        canvas.line(66,self.PAGE_HEIGHT-75,550,self.PAGE_HEIGHT-75)


        canvas.setFont('Times-Bold',16)
        canvas.drawString(228, self.PAGE_HEIGHT-108, self.Title)
        canvas.setFont('Times-Roman',9)
        canvas.drawString(inch, 0.75 * inch, "Page 1/ %s" % self.pageinfo)
        canvas.restoreState()

    def myLaterPages(self,canvas, doc):
        canvas.drawImage("img/home-blue.png", 36, 36)
        canvas.saveState()
        canvas.setStrokeColorRGB(0,0.4,1)
        canvas.setLineWidth(5)
        canvas.line(66,230,66,self.PAGE_HEIGHT-72)
        canvas.setFont('Times-Roman',9)
        canvas.drawString(inch, 0.75 * inch, "Page %d/ %s" % (doc.page, self.pageinfo))
        canvas.restoreState()

    def generate_pdf(self, output_filename):
        self.Elements.insert(0,Spacer(0,inch))
        doc = SimpleDocTemplate(output_filename)
        doc.build(self.Elements,onFirstPage=self.myFirstPage, onLaterPages=self.myLaterPages)
        return output_filename

    def header(self,txt, style=HeaderStyle, klass=Paragraph, sep=0.3):
        s = Spacer(0.25*inch, sep*inch)
        self.Elements.append(s)
        para = klass(txt, style)
        self.Elements.append(para)

    def header_small(self,txt, style=HeaderStyle_small, klass=Paragraph, sep=0.3):
        s = Spacer(0.13*inch, sep*inch)
        self.Elements.append(s)
        para = klass(txt, style)
        self.Elements.append(para)

    def p(self,txt):
        return self.header(txt, style=self.ParaStyle, sep=0.1)

    def pre(self,txt):
        s = Spacer(0.1*inch, 0.1*inch)
        self.Elements.append(s)
        p = Preformatted(txt, self.PreStyle)
        self.Elements.append(p)

    def page_break(self):
        self.Elements.append(PageBreak())

    def table(self,data):
        elements = []

        t=Table(data)
        t.setStyle(TableStyle([('BACKGROUND',(0,0),(-1,0),colors.gray),
                            ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                            ('VALIGN',(0,-1),(-1,-1),'MIDDLE'),
                            ('VALIGN',(0,0),(0,-1),'TOP'),
                            ('TEXTCOLOR',(0,0),(-1,0),colors.brown)]))

        self.Elements.append(t)

