from unittest import TestCase

from persistence_manager import PersistenceManager
from models.db_objects import *

__author__ = 'n0npax'


class TestPersistencManager(TestCase):

    def test_load_session(self):
        pm = PersistenceManager()
        session = PersistenceManager.load_session()
        self.assertNotEquals([], pm.metadata.tables.keys())

    def test_add_Obj_To_db(self):
        pm = PersistenceManager()
        session = PersistenceManager.load_session()

        pm.add_Obj_To_db(PropertyLocations,{'id':777,'clat':77.77,'clong':33.34, 'name' : 'test', 'city' : 'test', 'country' : 'pl'})
        obj = PersistenceManager.get_obj_by(PropertyLocations,'id',777)[0]
        self.assertEquals('test',obj.name)
        self.assertEquals('test',obj.city)

    def test_get_all_objects(self):
        pm = PersistenceManager()
        session = PersistenceManager.load_session()
        records = PersistenceManager.get_all_objects(PropertyLocations)
        for rec in records:
            self.assertEquals('test',rec.name)
            self.assertEquals('test',rec.city)

    def test_edit_object(self):
        pm = PersistenceManager()
        session = PersistenceManager.load_session()
        pm.add_Obj_To_db(PropertyLocations,{'id':10,'clat':77.77,'clong':33.34, 'name' : 'test1', 'city' : 'test', 'country' : 'pl'})
        obj = PersistenceManager.get_obj_by(PropertyLocations,'id',10)[0]
        obj.id = 90
        session.commit()
        self.assertEqual([],PersistenceManager.get_obj_by(PropertyLocations,'id',10))
        self.assertNotEqual([],PersistenceManager.get_obj_by(PropertyLocations,'id',90))
        pm.delete_object(obj)

    def test_delete_object(self):
        pm = PersistenceManager()
        session = PersistenceManager.load_session()
        for obj in PersistenceManager.get_all_objects(PropertyLocations):
            pm.delete_object(obj)
        self.assertEquals([],PersistenceManager.get_all_objects(PropertyLocations))

    def test_get_obj_by(self):
        pm = PersistenceManager()
        session = PersistenceManager.load_session()
        pm.add_Obj_To_db(PropertyLocations,{'id':111,'clat':77.77,'clong':33.34, 'name' : 'test1', 'city' : 'test', 'country' : 'pl'})
        pm.add_Obj_To_db(PropertyLocations,{'id':222,'clat':77.77,'clong':33.34, 'name' : 'test2', 'city' : 'test', 'country' : 'pl'})
        pm.add_Obj_To_db(PropertyLocations,{'id':333,'clat':77.77,'clong':33.34, 'name' : 'test3', 'city' : 'test', 'country' : 'pl'})
        obj222 = PersistenceManager.get_obj_by(PropertyLocations,'id',222)[0]
        obj111 = PersistenceManager.get_obj_by(PropertyLocations,'id',222,'<')[0]
        obj333 = PersistenceManager.get_obj_by(PropertyLocations,'id',222,'>')[0]
        self.assertEquals('test1',obj111.name)
        self.assertEquals('test2',obj222.name)
        self.assertEquals('test3',obj333.name)