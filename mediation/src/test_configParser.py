import shutil

from unittest import TestCase
from config_parser import ConfigParser

__author__ = 'n0npax'

class TestConfigParser(TestCase):

    def test_get_db_config(self):
        shutil.move('config.ini', 'config.ini.backup')
        test_config = '''
[db]
url = http://test:1234/test
username = test
password = test
database = test_db
'''

        with open('config.ini','w') as f:
            f.write(test_config)

        configuration = ConfigParser.get_db_config()
        self.assertEqual('http://test:1234/test',configuration['url'])
        self.assertEqual('test',configuration['username'])
        self.assertEqual('test_db',configuration['database'])
        self.assertEqual('test',configuration['password'])

        shutil.move('config.ini.backup','config.ini')


    def test_get_logs_config(self):
        shutil.move('config.ini', 'config.ini.backup')
        test_config = '''
[logs]
path = test
'''

        with open('config.ini','w') as f:
            f.write(test_config)

        configuration = ConfigParser.get_logs_config()
        self.assertEqual('test',configuration['path'])

        shutil.move('config.ini.backup','config.ini')


