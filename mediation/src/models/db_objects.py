from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy.ext.declarative import declarative_base

__author__ = 'n0npax'

Base = declarative_base()


class PropertyLocations(object):
    pass


class Devices(object):
    id = Column(Integer, primary_key=True)
    tag = Column('tag', String(25))
    name = Column('name', String(50))
    xmlDevDesc = Column('xmlDevDesc', String(300))
    devvalues = Column('devvalues', String(300))


class Devicevalues(object):
    pass

class CommunicationDialects(object):
    pass


class Buildings(object):
    pass

