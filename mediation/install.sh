#!/bin/bash
source installation/install_functions.sh


check_root
init_screen
echo "10" | dialog --gauge "Create dirs" 10 70 0
prepare_dirs
echo "20" | dialog --gauge "Copy sources" 10 70 0
copy_source
echo "30" | dialog --gauge "Copy 3rd party software" 10 70 0
copy_glassfish
echo "40" | dialog --gauge "Change access rights" 10 70 0
change_src_access_rights
echo "50" | dialog --gauge "Starting webserver" 10 70 0
restart_webserver
echo "60" | dialog --gauge "Add to systemd" 10 70 0
prepare_systemctl
echo "70" | dialog --gauge "Prepare firewall" 10 70 0
prepare_firewall
echo "80" | dialog --gauge "Prepare DB" 10 70 0
prepare_db
echo "90" | dialog --gauge "Deploing web on server" 10 70 0
deploy_web
echo "100" | dialog --gauge "Please wait" 10 70 0
show_ok
exit 0
