 --drop database masterOfHouse;
create database masterOfHouse;

-- create user ziwg;
-- grant all on masterOfHouse.* to 'zwig'@'localhost' identified by 'NEWPASSWORD';

use masterOfHouse;

-- DEFAULT CHARACTER SET utf8;
-- DEFAULT COLLATE utf8_general_ci;


CREATE TABLE `propertyLocations` (
	`id` integer(6) NOT NULL AUTO_INCREMENT,
   `name` varchar(10) NOT NULL,
   `clat` double NOT NULL,
   `clong` double NOT NULL,
   `country` varchar(25) NOT NULL,
   `city` varchar(25) NOT NULL, 
   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `buildings` (
   `id` int(10) NOT NULL AUTO_INCREMENT,
   
   `tag` varchar(25) NOT NULL,
   `name` varchar(50) NOT NULL,
   `clat` double NOT NULL,
   `clong` double NOT NULL,
   `open_hours` varchar(50) ,
  `address` varchar(160) , 
  `department` varchar(50) ,
  `directions` varchar(120) ,
  `photo` varchar(250) ,
  `description` varchar(600) ,
  `phone` varchar(16) ,
  `mail` varchar(50) ,
  `www` varchar(50) ,
   
  `propertyLocation` int(6) NOT NULL,
  `isConfirmed` boolean,
   
   PRIMARY KEY (`id`),
   FOREIGN KEY (propertyLocation) REFERENCES propertyLocations(id)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


 CREATE TABLE `communicationDialects` (
   `id` int(10) NOT NULL AUTO_INCREMENT,

   `tag` varchar(25) NOT NULL,
   `name` varchar(50) NOT NULL,
   `clat` double NOT NULL,
   `clong` double NOT NULL,
   `open_hours` varchar(50) ,
  `address` varchar(160) ,
  `floor` varchar(4) ,
  `room` varchar(6) ,
  `description` varchar(600) ,
  `phone` varchar(16) ,
  `mail` varchar(50) ,
  `www` varchar(50) ,

  `isConfirmed` boolean,
  `discriminator` varchar(2),

   PRIMARY KEY (`id`)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `aIs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

 CREATE TABLE `devices` (
   `id` int(10) NOT NULL AUTO_INCREMENT,

   `tag` varchar(25) NOT NULL,
   `name` varchar(50) NOT NULL,

  `editable` boolean ,
  `description` varchar(600) ,
  `devvalues` varchar(16) ,
  `xmlDevDesc` varchar(50) ,
  `www` varchar(50) ,
  `temperatureControlling` boolean,

  `communicationDialect` integer(6) NOT NULL ,
  `building` integer(6) NOT NULL ,
  `isConfirmed` boolean,
  `discriminator` varchar(2),

   PRIMARY KEY (`id`),
   FOREIGN KEY (communicationDialect) REFERENCES communicationDialects(id),
   FOREIGN KEY (building) REFERENCES buildings(id)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

 CREATE TABLE `deviceValues` (
   `id` int(10) NOT NULL AUTO_INCREMENT,
   
   `date` datetime NOT NULL,
   `devValues` text NOT NULL,
   `setByUser` boolean,
   
   `device` integer(6) NOT NULL,
   
   PRIMARY KEY (`id`),
   FOREIGN KEY (device) REFERENCES devices(id)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

 
 CREATE  TABLE users (
  username VARCHAR(45) NOT NULL ,
  password VARCHAR(45) NOT NULL ,
  enabled TINYINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (username));
  
  CREATE TABLE user_roles (
  user_role_id INT(11) NOT NULL AUTO_INCREMENT,
  username VARCHAR(45) NOT NULL,
  ROLE VARCHAR(45) NOT NULL,
  PRIMARY KEY (user_role_id),
  UNIQUE KEY uni_username_role (ROLE,username),
  KEY fk_username_idx (username),
  CONSTRAINT fk_username FOREIGN KEY (username) REFERENCES users (username));
 

INSERT INTO users(username,password,enabled)
VALUES ('n0npax','1a1dc91c907325c69271ddf0c944bc72', TRUE);
INSERT INTO users(username,password,enabled)
VALUES ('user','1a1dc91c907325c69271ddf0c944bc72', TRUE);
 
INSERT INTO user_roles (username, ROLE)
VALUES ('user', 'ROLE_USER');
INSERT INTO user_roles (username, ROLE)
VALUES ('n0npax', 'ROLE_ADMIN');
INSERT INTO user_roles (username, ROLE)
VALUES ('user', 'ROLE_MODERATOR');

insert into aIs values(1,'AI');
