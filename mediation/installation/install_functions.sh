#!/bin/bash
SOURCE_DIR="installation"
GLASSFISH_ASADMIN="/opt/masterOfHouse/glassfish4/bin/asadmin"
INSTALL_PATH="/opt/masterOfHouse"
GLASSFISH_ZIP="$SOURCE_DIR/glassfish.zip"
SERVICE_CTL_FILE="masterOfHouse.service"
SQL_CREATOR="$SOURCE_DIR/tables.sql"
DIALOG=${DIALOG=dialog}
WAR_PATH=""

function show_err
{
  $DIALOG --msgbox       "Installation aborted.  \n Exit code $1" 12 40
  exit $1
}

function show_ok
{
  $DIALOG --msgbox       "All OK. \n We hope software was installed correctly" 12 40
}


function check_root
{
  if [ "$EUID" -ne 0 ]
    then
    $DIALOG --msgbox      "Please use root account to install this software"  12 40
    exit 3
  fi
}

function prepare_dirs
{
  if [ -d "$INSTALL_PATH" ]; then

    $DIALOG --title " Installation " --clear \
        --yesno "Old installation Is located in $INSTALL_PATH.\nDo You want to remove old instance of masterOfHouse?" 20 50
    case $? in
    0)
        rm -rf $INSTALL_PATH/*;;
    1)
        clear;
        show_err 6;;
    255)
        clear;
        show_err 7;;
    esac
  else
    mkdir "$INSTALL_PATH"
  fi
}

function copy_glassfish
{
    unzip -q $GLASSFISH_ZIP -d $INSTALL_PATH
}

function copy_source
{
    cp src $INSTALL_PATH -rf
}

function prepare_systemctl
{
  cd $SOURCE_DIR
  cp $SERVICE_CTL_FILE /etc/systemd/system/
  chmod 755 /etc/systemd/system/$SERVICE_CTL_FILE
  systemctl enable /etc/systemd/system/$SERVICE_CTL_FILE
  systemctl start $SERVICE_CTL_FILE
  cd -
}

function prepare_firewall
{

  iptables -A INPUT -p tcp --dport 8080 -j ACCEPT
  iptables-save
  iptables-save | uniq | iptables-restore
}

function prepare_db
{
  data=$(tempfile 2>/dev/null)
 
  trap "rm -f $data" 0 1 2 5 15
 
  dialog --title "Password" --insecure --passwordbox "Enter your database root/admin password" 10 30 2> $data
 
  ret=$?
 
  case $ret in
    0)
       mysql -u root -p"$(cat $data)" -e exit
       if [ "$?" -ne 0 ]
         then
         $DIALOG --msgbox "Error. \n Wrong password" 12 40
         show_err 9
       fi;
       mysql -u root -p"$(cat $data)" < $SQL_CREATOR;;
    
    1)
      show_err 4;;
    255)
      [ -s $data ] &&  cat $data || echo "ESC pressed.";;
  esac
}

function change_src_access_rights
{
  chmod 644 $INSTALL_PATH/src -R
  chmod +x $INSTALL_PATH/src/run.py 
}

function init_screen
{
    $DIALOG --title " Installation " --clear \
        --yesno "Do You really want to install master of house on this computer?

Installator supports only mysql DB. If You're using other one, You need to manualy create database and tables.

Instalator will modify Your firewall and system settings.

Managment will be avialable via systemctl or script located in $INSTALL_PATH" 20 50

    case $? in
    0)
        ;;
    1)
        clear;
        show_err 1;;
    255)
        clear;
        show_err 2;;
    esac
}

function restart_webserver
{
  $GLASSFISH_ASADMIN  start-domain domain1 --terse=true
}

function deploy_web
{
  $GLASSFISH_ASADMIN  --force=true deploy $WAR_PATH `
}

