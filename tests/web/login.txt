*** Settings ***
Documentation     A test suite with a single test for valid login.
Resource          resource.txt
*** Test Cases ***
Check Login and Logout
    Valid Login and Logout  n0npax  pass

Check Wrong Pass
    Invalid Login  n0npax    wrong_pass

Check Wrong User
    Invalid Login  No_such_user    pass

Check Empty Pass
    Invalid Login  n0npax    ${EMPTY}

Check Empty User
    Invalid Login  ${EMPTY}    pass

Check Empty Pass and User
    Invalid Login  ${EMPTY}    ${EMPTY}

*** Keywords ***
Valid Login and Logout
    [Arguments]    ${user}    ${pass}
    Open Browser To Login Page
    Login Procedure    ${user}    ${pass}
    Welcome Page Should Be Open
    Logout Procedure
    Check If Logout Successed
    [Teardown]    Close Browser

Invalid Login
    [Arguments]   ${user}    ${pass}
    Open Browser To Login Page
    Input Username    ${user}
    Input Password    ${pass}
    Submit Credentials
    Login Fail Page Should be Open
    [Teardown]    Close Browser

 
