#-*- coding: utf-8 -*-
import sys    # sys.setdefaultencoding is cancelled by site.py
reload(sys)    # to re-enable sys.setdefaultencoding()
sys.setdefaultencoding('utf-8')

def Fun_X(html, key):
    LP, RP = html.find('[')+2, html.rfind(']')
    elements = html[LP:RP].split('}, {')

    ret = []
    for element in elements:
        lines = element.split('\n')
        i=0
        for line in lines:
            if key in line:
                return lines[i-1].split(':')[1].strip(', ')
            i+=1

    return ret


